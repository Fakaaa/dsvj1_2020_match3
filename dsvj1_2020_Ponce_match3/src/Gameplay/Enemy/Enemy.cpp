#include "Enemy.h"

#include <iostream>

#include "ScreenHandle/ScreenHandle.h"
#include "Gameplay/Scene/GameScene.h"
#include "Gameplay/Player/Player.h"
#include "Gameplay/GridManager/GridManager.h"
#include "Gameplay/gameHandle.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;

namespace Match3 {
	namespace EnemyHandle {
		//------------------------------
		ENEMY enemyMaze;
		//------------------------------
		bool ON_ATTACK;
		//-----------------
		int WIDTH_ENEMYS;
		int HEIGTH_ENEMYS;
		//-----------------
		int WIDTH_METTER;
		int HEIGTH_METTER;
		//-----------------
		float frameCounter = 0;
		float currentFrame = 0;
		float frameSpeed = 0;
		//-----------------
		int ANIME_STATE;
		//-----------------
		float timeBettewnAtacks;
		float actualTimeCharged;
		float timeToChargeMetter;
		//-----------------
		bool startAnimationTrans;
		bool startTranslate;
		int auxRow;
		//-----------------
		float posToTravel;
		float originalPos;
		//-----------------
		float loadMetter;
		//-----------------
		bool ATACKED;
		//===============================
		bool onRespawn;
		bool onRestartFromMenu = false;
		//======================================	
		void init(){
			//----------------------
			WIDTH_ENEMYS  = (int)(ScreenManager::actualRes.WIDTH / 6.0f);
			HEIGTH_ENEMYS = (int)(ScreenManager::actualRes.HEIGHT / 4.0f);
			WIDTH_METTER  = (int)(ScreenManager::actualRes.WIDTH / 12.4f);
			HEIGTH_METTER = (int)(ScreenManager::actualRes.HEIGHT / 30);
			//----------------------
			loadMetter = 0;
			posToTravel = (float)(ScreenManager::actualRes.WIDTH / 1.5f);
			originalPos = (float)(ScreenManager::actualRes.WIDTH / 1.2f);
			//----------------------
			enemyMaze.POS = { (float)(ScreenManager::actualRes.WIDTH / 1.2f),(float)(ScreenManager::actualRes.HEIGHT / 3.0f) };
			//----------------------
			if(!onRestartFromMenu)
				loadSprites();
			//----------------------
			enemyMaze.posBar = { (float)(enemyMaze.POS.x + ((enemyMaze.IDLE.height / colsIdleSprite)/2)),(float)(enemyMaze.POS.y + (enemyMaze.IDLE.height / rowsIdleSprite)) };
			//----------------------
			enemyMaze.posHBar = { (float)(enemyMaze.POS.x + ((enemyMaze.IDLE.height / colsIdleSprite) / 2)),(float)(enemyMaze.POS.y) };
			//----------------------
			enemyMaze.frameRec = { 0.0f,0.0f, (float)(enemyMaze.IDLE.width / 5),(float)(enemyMaze.IDLE.height / 4) };
			//-----------------------
			ANIME_STATE = 1;
			//-----------------------
			frameSpeed = 7;
			//-----------------------
			timeBettewnAtacks = 15.0f;
			//-----------------------
			enemyMaze.onAtack = false;
			//-----------------------
			//-----------------------
			startAnimationTrans = false;
			//-----------------------
			enemyMaze.chargerBar.width = 0;
			//-----------------------
			enemyMaze.healtBar.width = WIDTH_METTER;
			//-----------------------
			ATACKED = false;
			onRespawn = false;
			//-----------------------
			enemyMaze.alive = true;
			enemyMaze.state = WHITE;
			//-----------------------
		}
		//======================================
		void loadSprites(){
			//-----------
			Image resize;
			//-----------
			if (!ScreenManager::onResizeWindow) {
				//----------------------
				enemyMaze.damageMaded = 2;	//LO HAGO ACA PA QUE NO SE RESETE DESP DE RESIZE Y RECARGAR EL JUEGO
				//----------------------
				resize = LoadImage("res/assets/enemy/mazeIdle.png");
				ImageResize(&resize, WIDTH_ENEMYS * colsIdleSprite, HEIGTH_ENEMYS * rowsIdleSprite);
				ImageFlipHorizontal(&resize);
				enemyMaze.IDLE = LoadTextureFromImage(resize);
				UnloadImage( resize );
				//----------------------
				resize = LoadImage("res/assets/enemy/mazeAtack.png");
				ImageResize(&resize, WIDTH_ENEMYS * colsIdleSprite, HEIGTH_ENEMYS * rowsIdleSprite);
				ImageFlipHorizontal(&resize);
				enemyMaze.ATCK = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//========================
				resize = LoadImage("res/assets/enemy/chargebarEnemy.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.chargerMetter = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//----------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemyBAR.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.chargerBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------------------------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemy.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.healtBarUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//----------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemyBAR.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.healtBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//========================
			}
			else {
				//----------------------
				unloadSprites();
				//----------------------
				WIDTH_ENEMYS = (int)(ScreenManager::actualRes.WIDTH / 6.0f);
				HEIGTH_ENEMYS = (int)(ScreenManager::actualRes.HEIGHT / 4.0f);
				WIDTH_METTER = (int)(ScreenManager::actualRes.WIDTH / 12.4f);
				HEIGTH_METTER = (int)(ScreenManager::actualRes.HEIGHT / 30);
				//----------------------
				enemyMaze.POS = { (float)(ScreenManager::actualRes.WIDTH / 1.2f),(float)(ScreenManager::actualRes.HEIGHT / 3.0f) };
				posToTravel = (float)(ScreenManager::actualRes.WIDTH / 1.5f);
				originalPos = (float)(ScreenManager::actualRes.WIDTH / 1.2f);
				//----------------------
				resize = LoadImage("res/assets/enemy/mazeIdle.png");
				ImageResize(&resize, WIDTH_ENEMYS * colsIdleSprite, HEIGTH_ENEMYS * rowsIdleSprite);
				ImageFlipHorizontal(&resize);
				enemyMaze.IDLE = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//----------------------
				resize = LoadImage("res/assets/enemy/mazeAtack.png");
				ImageResize(&resize, WIDTH_ENEMYS * colsIdleSprite, HEIGTH_ENEMYS * rowsIdleSprite);
				ImageFlipHorizontal(&resize);
				enemyMaze.ATCK = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//========================
				resize = LoadImage("res/assets/enemy/chargebarEnemy.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.chargerMetter = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//----------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemyBAR.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.chargerBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------------------------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemy.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.healtBarUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//----------------------
				resize = LoadImage("res/assets/enemy/chargebarEnemyBAR.png");
				ImageResize(&resize, (int)(WIDTH_METTER), (int)(HEIGTH_METTER));
				ImageFlipHorizontal(&resize);
				enemyMaze.healtBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//========================
				enemyMaze.posBar = { (float)(enemyMaze.POS.x + ((enemyMaze.IDLE.height / colsIdleSprite) / 2)),(float)(enemyMaze.POS.y + (enemyMaze.IDLE.height / rowsIdleSprite)) };
				enemyMaze.posHBar = { (float)(enemyMaze.POS.x + ((enemyMaze.IDLE.height / colsIdleSprite) / 2)),(float)(enemyMaze.POS.y) };
				//----------------------
				enemyMaze.frameRec = { 0.0f,0.0f, (float)(enemyMaze.IDLE.width / 5),(float)(enemyMaze.IDLE.height / 4) };
				//----------------------
			}
			onRestartFromMenu = true;
		}
		//======================================
		static void translateEnemys() {
			//------------------------
			if (enemyMaze.POS.x >= posToTravel && startTranslate) {
				enemyMaze.POS.x-= 4;
			}
			if (enemyMaze.POS.x <= originalPos && !startTranslate) {
				enemyMaze.POS.x+= 4;
			}
			//------------------------
		}
		//======================================
		static void restartAnimationFrames(int animationToGo) {
			//-------------------------
			currentFrame = 0;
			auxRow = 0;
			ANIME_STATE = animationToGo;
			//-------------------------
		}
		//======================================
		static void calcMetterRecharge() {
			//--------------
			timeToChargeMetter += GetFrameTime();
			
			if (timeToChargeMetter >= 1.5f) {
				if (enemyMaze.chargerBar.width < WIDTH_METTER)
					enemyMaze.chargerBar.width += (int)(ScreenManager::actualRes.WIDTH / 120);
				else if (enemyMaze.chargerBar.width >= WIDTH_METTER)
					enemyMaze.chargerBar.width = WIDTH_METTER;

				timeToChargeMetter = 0;
			}
			//--------------
		}
		//======================================
		static void enemyDead() {
			//-------------
			bool itNeedsGo = false;
			//-------------
			if (enemyMaze.healtBar.width <= (float)(ScreenManager::actualRes.WIDTH / 62)) {

				if (enemyMaze.state.a >= 0)
					enemyMaze.state.a--;
				if(enemyMaze.state.a <= 0) {
					enemyMaze.state.a = 0;
				}

				if (enemyMaze.POS.x < ScreenManager::actualRes.WIDTH + WIDTH_ENEMYS) {
					enemyMaze.POS.x += 1;
				}
				else if (enemyMaze.POS.x >= (ScreenManager::actualRes.WIDTH + WIDTH_ENEMYS)) {
					itNeedsGo = true;
				}
				if (itNeedsGo && enemyMaze.POS.x > originalPos) {
					enemyMaze.alive = false;
					onRespawn = true;
				}
				frameSpeed = 0;
			}
			//-------------
		}
		//======================================
		static void playerAtacking() {
			//------------------------
			GridHandle::gridBLOCKED = false;
			ON_ATTACK = false;
			ANIME_STATE = 1;
			actualTimeCharged = 0;
			timeToChargeMetter = 0;
			enemyMaze.chargerBar.width = 1;
			enemyMaze.onAtack = false;
			startTranslate = false;
			startAnimationTrans = false;
			//------------------------
			if (ATACKED) {
				enemyMaze.healtBar.width -= Player::pj.group.carry.dps;
				SoundsHandle::stopSound(SoundsHandle::playerAttack.sfx);
				SoundsHandle::playSound(SoundsHandle::playerAttack.sfx);
				ATACKED = false;
			}
			//------------------------
		}
		//======================================
		void calcTimeToAtck(){
			//--------------------
			if (actualTimeCharged <= timeBettewnAtacks && !Player::pj.group.carry.doAtck) {
				actualTimeCharged += GetFrameTime();
				calcMetterRecharge();
				std::cout << "ENEMY TIME TO ATACK:" << actualTimeCharged << std::endl;
			}
			else if(actualTimeCharged > timeBettewnAtacks){
				enemyMaze.onAtack = true;
			}
			else if (Player::pj.group.carry.doAtck) {
				playerAtacking();
			}
			//--------------------
			if (actualTimeCharged >= 10 && actualTimeCharged <= 12) {
				startTranslate = true;
				ON_ATTACK = true;
				startAnimationTrans = true;
			}
			//--------------------
			if (startAnimationTrans) {
				restartAnimationFrames(2);
				startAnimationTrans = false;
				SoundsHandle::stopSound(SoundsHandle::damageEnemy.sfx);
				SoundsHandle::playSound(SoundsHandle::damageEnemy.sfx);
			}
			//--------------------
			translateEnemys();
			//--------------------
			enemyDead();
		}
		//======================================
		void upadate(){
			//-----------------
			//DESCOMENTAR SI QUERES MATAR AL ENEMIGO DE UNA,
			//ADVERTENCIA: NO FUNCIONA BIEN LA ANIMACION DEL PLAYER CON ESTO
			//if (IsKeyPressed(KEY_A)) {
			//	Player::pj.group.carry.doAtck = true;
			//	ATACKED = true;
			//}
			//-----------------
			if (!SceneHandle::onPause && enemyMaze.alive && !onRespawn){
				calcTimeToAtck();  
				updateEnemysSprite();
			}
			if(!enemyMaze.alive && enemyMaze.POS.x >= originalPos && onRespawn){
				//---
				generateAnotherEnemy();
				//---
			}
			//-----------------
		}
		//======================================
		void generateAnotherEnemy(){
			//-------------------
			enemyMaze.state = WHITE;

			if (enemyMaze.POS.x >= originalPos) {
				GameHandle::makingTransition = true;
				enemyMaze.POS.x--;
			}
			if(enemyMaze.POS.x <= (originalPos + 4)){
				enemyMaze.alive = true;
				onRespawn = false;
				enemyMaze.chargerBar.width = 1;
				enemyMaze.healtBar.width = WIDTH_METTER;
				enemyMaze.onAtack = false;
				ANIME_STATE = 1;
			}
			//-------------------
		}
		//======================================
		void calcFrameAnim(Rectangle & frameRec, Texture2D& spriteCalc,const int & frameCols, const int & frameRows){
			//------------------------------------------
			frameCounter++;

			if (frameCounter >= (ScreenManager::maxFPS / frameSpeed)) {
				frameCounter = 0;
				currentFrame++;

				if (auxRow >= frameRows) {
					auxRow = 0;
					currentFrame = 0;
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
					frameRec.y = (float)auxRow*(float)((spriteCalc.height / frameRows));
				}

				if (frameRec.x <= (frameCols * WIDTH_ENEMYS)) {
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
				}
				else {
					auxRow++;
					currentFrame = 0;
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
					frameRec.y = (float)auxRow*(float)((spriteCalc.height / frameRows));
				}
			}
			if (frameSpeed >= MAX_FRAME_SPEED) frameSpeed = MAX_FRAME_SPEED;
			else if (frameSpeed <= MIN_FRAME_SPEED) frameSpeed = MIN_FRAME_SPEED;
			//------------------------------------------
		}
		//======================================
		void updateEnemysSprite(){
			//---------
			switch (ANIME_STATE)
			{
			case 1:
				calcFrameAnim(enemyMaze.frameRec, enemyMaze.IDLE , colsIdleSprite, rowsIdleSprite);
				break;
			case 2:
				calcFrameAnim(enemyMaze.frameRec, enemyMaze.ATCK , colsIdleSprite, rowsIdleSprite);
				break;
			}
			//---------
		}
		//======================================
		void drawEnemy(){
			//----------------------
			switch (ANIME_STATE)
			{
			case 1:		
				if (!Player::pj.group.debuffEnemy)
					DrawTextureRec(enemyMaze.IDLE, enemyMaze.frameRec,	enemyMaze.POS, enemyMaze.state);
				else
					DrawTextureRec(enemyMaze.IDLE, enemyMaze.frameRec,	enemyMaze.POS, YELLOW);
				break;
			case 2:		
				if(!Player::pj.group.debuffEnemy)
					DrawTextureRec(enemyMaze.ATCK, enemyMaze.frameRec, enemyMaze.POS, enemyMaze.state);
				else
					DrawTextureRec(enemyMaze.ATCK, enemyMaze.frameRec, enemyMaze.POS, YELLOW);
				break;
			}
			//----------------------
			DrawTextureEx(enemyMaze.chargerBar, enemyMaze.posBar, 0.0f, 1.0f, SKYBLUE);
			//----------------------
			DrawTextureEx(enemyMaze.chargerMetter, enemyMaze.posBar, 0.0f, 1.0f, WHITE);
			//----------------------
			
			//----------------------
			DrawTextureEx(enemyMaze.healtBar, enemyMaze.posHBar, 0.0f, 1.0f, GREEN);
			//----------------------
			DrawTextureEx(enemyMaze.healtBarUX, enemyMaze.posHBar, 0.0f, 1.0f, WHITE);
			//----------------------
		}
		//======================================
		void deinit(){
			//-------------------
			unloadSprites();
			//-------------------
		}
		//======================================
		void unloadSprites(){
			//---------------------
			UnloadTexture( enemyMaze.IDLE );
			//---------------------
			UnloadTexture( enemyMaze.ATCK );
			//---------------------
			UnloadTexture( enemyMaze.chargerMetter );
			//---------------------
			UnloadTexture( enemyMaze.chargerBar );
			//---------------------
			UnloadTexture( enemyMaze.healtBarUX );
			//---------------------
			UnloadTexture( enemyMaze.healtBar );
			//---------------------
		}
		//======================================
	}
}