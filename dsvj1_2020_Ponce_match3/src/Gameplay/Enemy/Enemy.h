#ifndef ENEMY_H
#define ENEMY_H

#include "raylib.h"

#define MAX_FRAME_SPEED 20
#define MIN_FRAME_SPEED 7

namespace Match3 {
	namespace EnemyHandle {
		//-----------------
		//-----------------
		const int colsIdleSprite = 5;
		const int rowsIdleSprite = 4;
		//-----------------
		struct ENEMY{
			//--------
			Texture2D IDLE;
			Texture2D ATCK;
			Rectangle frameRec;
			Vector2 POS;
			//--------
			bool onAtack;
			Color state;
			bool alive;
			//--------
			Texture2D chargerMetter;
			Texture2D chargerBar;
			Vector2 posBar;

			Texture2D healtBarUX;
			Texture2D healtBar;
			Vector2 posHBar;
			//--------

			int damageMaded;
		};
		//-----------------
		extern ENEMY enemyMaze;
		//-----------------
		extern bool ON_ATTACK;
		//-----------------
		extern float timeBettewnAtacks;
		extern float actualTimeCharged;
		extern int ANIME_STATE;
		extern bool startTranslate;
		//-----------------
		extern bool ATACKED;
		//-----------------
		void init();
		//-----------------
		void loadSprites();
		//-----------------
		void calcTimeToAtck();
		//-----------------
		void upadate();
		//-----------------
		void generateAnotherEnemy();
		//-----------------
		void calcFrameAnim(Rectangle & frameRec, Texture2D& spriteCalc, const int & frameCols, const int & frameRows);
		//-----------------
		void updateEnemysSprite();
		//-----------------
		void drawEnemy();
		//-----------------
		void deinit();
		//-----------------
		void unloadSprites();
		//-----------------
	}
}
#endif