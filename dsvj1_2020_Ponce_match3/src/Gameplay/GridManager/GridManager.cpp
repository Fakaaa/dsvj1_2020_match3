#include <iostream>

#include "GridManager.h"

#include "ScreenHandle/ScreenHandle.h"
#include "Gameplay/Player/Player.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;
using namespace std;

namespace Match3 {
	namespace GridHandle {
		//------------
		bool gridBLOCKED;
		float APLHAGRIDX;
		float APLHAGRIDY;
		//------------
		GRID GAME_GRID[size_X][size_Y];
		//------------
		static Vector2 START_GRIDPOS;
		static float WIDTH_SHAPES;
		static float HEIGHT_SHAPES;
		static Color colours[colores] = {RED,GREEN,BLUE,VIOLET,YELLOW};
		static Texture2D whatTexture[GEMS];
		static short half;
		static float halfShapes = 1.8f;
		//------------
		float MULTIPLER_GEM;
		float SCALE_METTER;
		//------------
		TYPE_SHAPE AUX_COLOR;
		//------------
		//=======================
		static bool itHappend = false;

		static POS AUX_POS = { 0 , 0};
		//=======================
		static int countMatch = 0;
		//=======================
		//------------
		bool onUpdateGrid = false;
		//------------
		bool comeBack = false;
		//------------
		POS AUX_POSITIONS[TAMAUX];
		//------------
		bool exitForAux;
		//------------
		bool SCAlE_METTER_MAX;
		//------------
		bool alreadyCharged;
		//------------
		int forGemRandom;
		//------------
		void initGridOnStart(){
			//---------
			SCALE_METTER = 0;
			SCAlE_METTER_MAX = false;
			gridBLOCKED = false;
			forGemRandom = 0;
			//---------
			half = 5;
			//---------
			WIDTH_SHAPES = static_cast<float>(ScreenManager::actualRes.WIDTH / 19.0f);	 //65
			HEIGHT_SHAPES = static_cast<float>(ScreenManager::actualRes.HEIGHT / 12.0f); //60
			//---------
			START_GRIDPOS = { static_cast<float>((ScreenManager::actualRes.WIDTH / half) - ((WIDTH_SHAPES / halfShapes) * size_X)),
			static_cast<float>(ScreenManager::actualRes.HEIGHT / 35) };
			//---------

			loadTextures();
			//---------
			AUX_POS = { 0 , 0 };
			//AUX_POSITIONS.push_back(AUX_POS);

			for (int i = 0; i < TAMAUX; i++){
				AUX_POSITIONS[i].x = 0;
				AUX_POSITIONS[i].y = 0;
				AUX_POSITIONS[i].ONTHATAUX = false;
			}

			//---------
			for (int i = 1; i <= size_X; i++){
				for (int j = 1; j <= size_Y; j++){
					//-------------		 SCALES
					//MATRIX
					GAME_GRID[i-1][j-1].gridShape.width = WIDTH_SHAPES;
					GAME_GRID[i-1][j-1].gridShape.height = HEIGHT_SHAPES;
					//-------------
					//SHAPES
					GAME_GRID[i-1][j-1].shapes.width = WIDTH_SHAPES - 10;
					GAME_GRID[i-1][j-1].shapes.height = HEIGHT_SHAPES - 10;
					//-------------		POSITIONS
					//MATRIX
					GAME_GRID[i-1][j-1].gridShape.x = START_GRIDPOS.x + (i * WIDTH_SHAPES);
					GAME_GRID[i-1][j-1].gridShape.y = START_GRIDPOS.y + (j * HEIGHT_SHAPES);
					GAME_GRID[i - 1][j - 1].position = { i - 1 , j - 1 };
					//-------------
					//SHAPES
					GAME_GRID[i-1][j-1].shapes.x = START_GRIDPOS.x + (i * WIDTH_SHAPES + 5) ;
					GAME_GRID[i-1][j-1].shapes.y = START_GRIDPOS.y + (j * HEIGHT_SHAPES + 5);
					//-------------		  COLORS
					//COLORS
					//GAME_GRID[i-1][j-1].whatShape = (TYPE_SHAPE)GetRandomValue(0,4);
					forGemRandom = GetRandomValue(0, 60);

					if (forGemRandom <= 20)
						GAME_GRID[i - 1][j - 1].whatShape = red;
					else if (forGemRandom > 20 && forGemRandom <= 30)
						GAME_GRID[i - 1][j - 1].whatShape = green;
					else if (forGemRandom > 30 && forGemRandom <= 40)
						GAME_GRID[i - 1][j - 1].whatShape = blue;
					else if (forGemRandom > 40 && forGemRandom <= 50)
						GAME_GRID[i - 1][j - 1].whatShape = violet;
					else if (forGemRandom > 50 && forGemRandom <= 60)
						GAME_GRID[i - 1][j - 1].whatShape = yellow;


					GAME_GRID[i-1][j-1].colorShape = colours[ GAME_GRID[i-1][j-1].whatShape ];
					GAME_GRID[i-1][j-1].canSelect = false;
					//-------------	
					#if DEBUG
						std::cout << "POS GRID AUX [" << i << "] [" << j << "] " << std::endl;
						std::cout << "POS GRID [" << i << "] [" << j << "] " << std::endl;
					#endif
					//-------------	
					GAME_GRID[i - 1][j - 1].state = true;
					//-------------	
				}
			}
			//---------
			AUX_COLOR = nothing;
		}
		//------------
		void drawGrid(){
			//--------------------------------------
			for (int i = 0; i < size_X; i++){
				for (int j = 0; j < size_Y; j++){
					//----------------------
					switch (GAME_GRID[i][j].whatShape)	
					{
					case red:
						if(!gridBLOCKED)
							DrawTexture(whatTexture[0], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), GAME_GRID[i][j].colorShape);
						else
							DrawTexture(whatTexture[0], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), ALPHAGRID);
						break;
					case green:
						if (!gridBLOCKED)
							DrawTexture(whatTexture[1], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), GAME_GRID[i][j].colorShape);
						else
							DrawTexture(whatTexture[1], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), ALPHAGRID);
						break;
					case blue:
						if (!gridBLOCKED)
							DrawTexture(whatTexture[2], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), GAME_GRID[i][j].colorShape);
						else
							DrawTexture(whatTexture[2], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), ALPHAGRID);
						break;
					case violet:
						if (!gridBLOCKED)
							DrawTexture(whatTexture[3], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), GAME_GRID[i][j].colorShape);
						else
							DrawTexture(whatTexture[2], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), ALPHAGRID);
						break;
					case yellow:
						if (!gridBLOCKED)
							DrawTexture(whatTexture[4], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), GAME_GRID[i][j].colorShape);
						else
							DrawTexture(whatTexture[2], (int)(GAME_GRID[i][j].shapes.x), (int)(GAME_GRID[i][j].shapes.y), ALPHAGRID);
						break;
					}
					//----------------------
				}
			}
			//--------------------------------------
		}
		//------------
		static void scaleMetterPlayer(bool& alreadyChaged) {

			if (SCALE_METTER <= Player::HEIGTH_METTERBAR) {

				if (Player::swipeCountShapes >= 3 && Player::swipeCountShapes <= 6) {
					SCALE_METTER = (float)((ScreenManager:: actualRes.WIDTH / 120) * Player::swipeCountShapes);
					if ((Player::pj.group.carry.atckMetterBar.height + SCALE_METTER) <= Player::HEIGTH_METTERBAR && !alreadyChaged) {
						Player::pj.group.carry.atckMetterBar.height += static_cast<int>(SCALE_METTER);
						alreadyChaged = true;
					}
					else if ((Player::pj.group.carry.atckMetterBar.height + SCALE_METTER) > Player::HEIGTH_METTERBAR && !alreadyChaged) {
						Player::pj.group.carry.atckMetterBar.height = Player::HEIGTH_METTERBAR;
						SCAlE_METTER_MAX = true;
						alreadyChaged = true;
					}
				}
				else if(Player::swipeCountShapes >= 6 && Player::swipeCountShapes <= (size_X * size_Y)){
					SCALE_METTER = (float)((ScreenManager::actualRes.WIDTH / 120)* Player::swipeCountShapes);
					if ((Player::pj.group.carry.atckMetterBar.height + SCALE_METTER) <= Player::HEIGTH_METTERBAR && !alreadyChaged) {
						Player::pj.group.carry.atckMetterBar.height += static_cast<int>(SCALE_METTER);
						alreadyChaged = true;
					}
					else if ((Player::pj.group.carry.atckMetterBar.height + SCALE_METTER) > Player::HEIGTH_METTERBAR && !alreadyChaged) {
						Player::pj.group.carry.atckMetterBar.height = Player::HEIGTH_METTERBAR;
						SCAlE_METTER_MAX = true;
						alreadyChaged = true;
					}
				}
			}
		}
		//------------
		static void whatActionPerGem(int& i, int& j, bool& metterCharged) {
			//--------------
			static int SIZE_X = size_X;
			static int SIZE_Y = size_Y;
			//--------------
			switch (GAME_GRID[i][j].whatShape)
			{
			case red:
				scaleMetterPlayer(metterCharged);
				break;
			case green:
				Player::healTeam();
				break;
			case blue:
				Player::shieldTeam(SIZE_X, SIZE_Y);
				break;
			case violet:
				Player::anticipateBoostX2 = true;
				Player::usePurpleGems(SIZE_X, SIZE_Y);
				break;
			case yellow:
				Player::useYellowGems(SIZE_X, SIZE_Y);
				break;
			}
			//--------------
		}
		//------------
		void updateGrid(){
			//-------------------- ACTUALLY UPDATE WHERE WE SET IN FALSE THE SHAPE
			for (int i = 0; i < size_X; i++){
				for (int j = 0; j < size_Y; j++){
					//=========================
					if (GAME_GRID[i][j].onSelec && Player::swipeCountShapes >= 3) {
						//=============================
						whatActionPerGem(i, j, alreadyCharged);
						//=============================
						if (GAME_GRID[i][j + 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i][j+1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i][j + 1].state = false;
						}
						else if (GAME_GRID[i][j - 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i][j - 1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i][j - 1].state = false;
						}
						if (GAME_GRID[i + 1][j].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i+1][j].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i + 1][j].state = false;
						}
						else if (GAME_GRID[i - 1][j].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i-1][j].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i - 1][j].state = false;
						}
						//=============================
						if (GAME_GRID[i - 1][j + 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i - 1][j + 1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i - 1][j + 1].state = false;
						}
						else if (GAME_GRID[i + 1][j + 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i + 1][j + 1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i + 1][j + 1].state = false;
						}
						if (GAME_GRID[i - 1][j - 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i - 1][j - 1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i - 1][j - 1].state = false;
						}
						else if (GAME_GRID[i + 1][j - 1].whatShape == GAME_GRID[i][j].whatShape && GAME_GRID[i + 1][j - 1].onSelec) {
							GAME_GRID[i][j].state = false;
							GAME_GRID[i + 1][j - 1].state = false;
						}
						//================================
						if (!GAME_GRID[i][j].state) {
							GAME_GRID[i][j].colorShape = BLACK;
							GAME_GRID[i][j].whatShape = black;
							GAME_GRID[i][j].onSelec = false;
						}
					}
					else {
						switch (GAME_GRID[i][j].whatShape)
						{
						case red:
							GAME_GRID[i][j].colorShape = RED;
							GAME_GRID[i][j].onSelec = false;
							break;
						case green:
							GAME_GRID[i][j].colorShape = GREEN;
							GAME_GRID[i][j].onSelec = false;
							break;
						case blue:
							GAME_GRID[i][j].colorShape = BLUE;
							GAME_GRID[i][j].onSelec = false;
							break;
						case violet:
							GAME_GRID[i][j].colorShape = VIOLET;
							GAME_GRID[i][j].onSelec = false;
							break;
						case yellow:
							GAME_GRID[i][j].colorShape = YELLOW;
							GAME_GRID[i][j].onSelec = false;
							break;
						}
					}
					//=========================
				}
			}
			//-------------------- UPDATE FOR COLOR AND GEN ANOTHER SHAPE
			for (int i = size_X -1; i >= 0; i--){
				for (int j = size_Y-1; j >= 0; j--){
					//-----
					if (GAME_GRID[i][j].whatShape == black && (GAME_GRID[i][j].shapes.y != GAME_GRID[i][0].shapes.y)) {
						GAME_GRID[i][j].colorShape = GAME_GRID[i][j-1].colorShape;
						GAME_GRID[i][j].whatShape = GAME_GRID[i][j-1].whatShape;
						GAME_GRID[i][j-1].colorShape = BLACK;
						GAME_GRID[i][j - 1].whatShape = black;
					}
					else if ((GAME_GRID[i][j].shapes.y == GAME_GRID[i][0].shapes.y) && GAME_GRID[i][j].whatShape == black) {
						//GAME_GRID[i][j].whatShape = (TYPE_SHAPE)GetRandomValue(0, 4);
						forGemRandom = GetRandomValue(0, 60);

						if (forGemRandom <= 20)
							GAME_GRID[i][j].whatShape = red;
						else if (forGemRandom > 20 && forGemRandom <= 30)
							GAME_GRID[i][j].whatShape = green;
						else if (forGemRandom > 30 && forGemRandom <= 40)
							GAME_GRID[i][j].whatShape = blue;
						else if (forGemRandom > 40 && forGemRandom <= 50)
							GAME_GRID[i][j].whatShape = violet;
						else if (forGemRandom > 50 && forGemRandom <= 60)
							GAME_GRID[i][j].whatShape = yellow;
						GAME_GRID[i][j].colorShape = colours[GAME_GRID[i][j].whatShape];
					}
				}
			}
			//--------------------
			Player::swipeCountShapes = 0;
			Player::pj.group.debuffEnemy = false;
			//--------------------

			onUpdateGrid = false;
			itHappend = false;
			alreadyCharged = false;
			AUX_COLOR = nothing;
			countMatch = 0;
			for (int i = 0; i < size_X; i++) {
				for (int j = 0; j < size_Y; j++){
					GAME_GRID[i][j].canSelect = false;
				}
			}
		}
		//------------
		static void returnToPosition(int& selectedI, int& selectedJ, Color& colorGem) {
			//---------------
			int savePosOnArr;
			int auxTileSelected = Player::swipeCountShapes;
			//---------------
			for (int k = 0; k < Player::swipeCountShapes; k++)
			{
				if (GAME_GRID[selectedI][selectedJ].position.x == AUX_POSITIONS[k].x &&
					GAME_GRID[selectedI][selectedJ].position.y == AUX_POSITIONS[k].y) {
					savePosOnArr = k;
				}
			}
			//---------------
			for (int k = savePosOnArr + 1; k < auxTileSelected; k++)
			{
				for (int i = 0; i < size_X; i++)
				{
					for (int j = 0; j < size_Y; j++)
					{
						if (GAME_GRID[i][j].position.x == AUX_POSITIONS[k].x &&
							GAME_GRID[i][j].position.y == AUX_POSITIONS[k].y) {
							GAME_GRID[i][j].onSelec = false;
							GAME_GRID[i][j].colorShape = colorGem;
							Player::swipeCountShapes--;
							SoundsHandle::stopSound(SoundsHandle::gemSelect.sfx);
							SoundsHandle::playSound(SoundsHandle::gemSelect.sfx);
						}
					}
				}
			}
		}
		//------------
		static bool checkOnArray(int& i, int& j) {
			//-----------------
			for (int k = 0; k < Player::swipeCountShapes; k++)
			{
				if (i == AUX_POSITIONS[k].x &&
					j == AUX_POSITIONS[k].y) {
					return true;
				}
			}
			return false;
			//-----------------
		}
		//------------
		static void calcPositionSelected(int& i , int& j, Color& toChangeAlpha, Color& toOriginColor) {
			//---------------------------------------
			if ((GAME_GRID[i][j].canSelect || countMatch == 0) && !GAME_GRID[i][j].onSelec) {
				GAME_GRID[i][j].colorShape = toChangeAlpha;
				GAME_GRID[i][j].onSelec = true;

				AUX_POSITIONS[Player::swipeCountShapes].x = GAME_GRID[i][j].position.x;
				AUX_POSITIONS[Player::swipeCountShapes].y = GAME_GRID[i][j].position.y;

				Player::swipeCountShapes++;

				SoundsHandle::stopSound(SoundsHandle::gemSelect.sfx);
				SoundsHandle::playSound(SoundsHandle::gemSelect.sfx);

				countMatch++;
			}
			if (GAME_GRID[i][j].onSelec && checkOnArray(i, j)) {
				returnToPosition(i, j, toOriginColor);
			}
			//---------------------------------------
		}
		//------------
		static void calcColor(int& i, int& j) {
			//----------------------
			switch (GAME_GRID[i][j].whatShape)
			{
			case red:
				//------------
				static Color alphaRed = REDALPHA;
				static Color originRed = RED;
				if (AUX_COLOR == nothing)
					AUX_COLOR = red;
				else if (AUX_COLOR != red) {
					break;
				}
				//------------
				calcPositionSelected(i, j, alphaRed, originRed);
				//------------
				break;
			case green:
				static Color alphaGreen = GREENALPHA;
				static Color originGreen = GREEN;
				if (AUX_COLOR == nothing)
					AUX_COLOR = green;
				else if (AUX_COLOR != green) {
					break;
				}
				//------------
				calcPositionSelected(i, j, alphaGreen, originGreen);
				//------------
				break;
			case blue:
				static Color alphaBlue = BLUEALPHA;
				static Color originBlue = BLUE;
				if (AUX_COLOR == nothing)
					AUX_COLOR = blue;
				else if (AUX_COLOR != blue) {
					break;
				}
				//------------
				calcPositionSelected(i, j, alphaBlue, originBlue);
				//------------
				break;
			case violet:
				static Color alphaViolet = VIOLETALPHA;
				static Color originViolet = VIOLET;
				if (AUX_COLOR == nothing)
					AUX_COLOR = violet;
				else if (AUX_COLOR != violet) {
					break;
				}
				//------------
				calcPositionSelected(i, j, alphaViolet, originViolet);
				//------------
				break;
			case yellow:
				//------------
				static Color alphaYellow = YELLOWALPHA;
				static Color originYellow = YELLOW;
				if (AUX_COLOR == nothing)
					AUX_COLOR = yellow;
				else if (AUX_COLOR != yellow) {
					break;
				}
				//------------
				calcPositionSelected(i, j, alphaYellow, originYellow);
				//------------
				break;
			}
			//----------------------
		}
		//------------
		void checkGridPlayer(){
			//-----------------------------
			if (!onUpdateGrid) {
				//-----------------------------
				for (int i = 0; i < size_X; i++){
					for (int j = 0; j < size_Y; j++){

						if (CheckCollisionPointRec(Player::pj.POS_MOUSE, GAME_GRID[i][j].shapes) && Player::swiping && (countMatch == 0 || GAME_GRID[i][j].canSelect)) {

							#if DEBUG
							//std::cout << "Toco la posicion: [" << GAME_GRID[i][j].position.x << "] [" << GAME_GRID[i][j].position.y << "]" << std::endl;
							//std::cout << "Can selec: [" << GAME_GRID[i][j].canSelect << "]"<< std::endl;
							//std::cout << "Posicion AUX: [" << AUX_POS.x << "] [" << AUX_POS.y << "]" << std::endl;
							#endif

							//===============================================
								//===================================
							if (j + 1 < size_Y && 
								GAME_GRID[i][j + 1].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i][j + 1].canSelect = true;

								itHappend = true;
							}
							if (j - 1 >= 0 && 
								GAME_GRID[i][j - 1].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i][j - 1].canSelect = true;

								itHappend = true;
							}
							if (i + 1 < size_X && 
								GAME_GRID[i + 1][j].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i + 1][j].canSelect = true;

								itHappend = true;
							}
							if (i -1 >= 0 && 
								GAME_GRID[i - 1][j].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i - 1][j].canSelect = true;

								itHappend = true;
							}
							if (i - 1 >= 0 && j + 1 < size_Y && 
								GAME_GRID[i - 1][j + 1].whatShape == GAME_GRID[i][j].whatShape){
									GAME_GRID[i - 1][j + 1].canSelect = true;

								itHappend = true;
							}
							if (i + 1 < size_X && j - 1 >= 0 && 
								GAME_GRID[i + 1][j - 1].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i + 1][j - 1].canSelect = true;

								itHappend = true;
							}
							if (i - 1 >= 0 && j - 1 >= 0 && 
								GAME_GRID[i - 1][j - 1].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i - 1][j - 1].canSelect = true;

								itHappend = true;
							}
							if (i + 1 < size_X && j + 1 < size_Y && 
								GAME_GRID[i + 1][j + 1].whatShape == GAME_GRID[i][j].whatShape) {
									GAME_GRID[i + 1][j + 1].canSelect = true;

								itHappend = true;
							}
							//===================================
							if(itHappend) calcColor(i,j);
							//===============================================
						}
					}
				}
			}
			else {
				//----------
				updateGrid();
				//----------
			}
			//-----------------------------
		}
		//------------
		void resizeGrid(){
			//------------
			WIDTH_SHAPES = static_cast<float>(ScreenManager::actualRes.WIDTH / 19.0f);
			HEIGHT_SHAPES = static_cast<float>(ScreenManager::actualRes.HEIGHT / 12.0f);
			//------------
			SCALE_METTER = Player::HEIGTH_METTERBAR;
			//------------
			START_GRIDPOS = { static_cast<float>((ScreenManager::actualRes.WIDTH / half) - ((WIDTH_SHAPES / halfShapes) * size_X)),
			static_cast<float>(20) };
			//---------
			for (int i = 1; i <= size_X; i++) {
				for (int j = 1; j <= size_Y; j++) {
					//-------------		 SCALES
					//MATRIX
					GAME_GRID[i - 1][j - 1].gridShape.width = WIDTH_SHAPES;
					GAME_GRID[i - 1][j - 1].gridShape.height = HEIGHT_SHAPES;
					//-------------
					//SHAPES
					GAME_GRID[i - 1][j - 1].shapes.width = WIDTH_SHAPES - 10;
					GAME_GRID[i - 1][j - 1].shapes.height = HEIGHT_SHAPES - 10;
					//-------------		POSITIONS
					//MATRIX
					GAME_GRID[i - 1][j - 1].gridShape.x = START_GRIDPOS.x + (i * WIDTH_SHAPES);
					GAME_GRID[i - 1][j - 1].gridShape.y = START_GRIDPOS.y + (j * HEIGHT_SHAPES);
					//-------------
					//SHAPES
					GAME_GRID[i - 1][j - 1].shapes.x = START_GRIDPOS.x + (i * WIDTH_SHAPES + 5);
					GAME_GRID[i - 1][j - 1].shapes.y = START_GRIDPOS.y + (j * HEIGHT_SHAPES + 5);
					//-------------		  COLORS
					//COLORS
					GAME_GRID[i - 1][j - 1].whatShape = (TYPE_SHAPE)GetRandomValue(0, 4);
					GAME_GRID[i - 1][j - 1].colorShape = colours[GAME_GRID[i - 1][j - 1].whatShape];
					//-------------
				}
			}
			//------------
			if(ScreenManager::onResizeWindow)loadTextures();
			//------------
		}
		//------------
		void loadTextures(){
			Image resizeTexture;

			if (!ScreenManager::onResizeWindow) {
				//======================
				// GEMS LOAD NORMAL
				for (int i = 0; i < GEMS; i++){
					resizeTexture = LoadImage(FormatText("res/assets/gridgems/gem%i.png",i));
					ImageResize(&resizeTexture, (int)WIDTH_SHAPES, (int)HEIGHT_SHAPES);
					whatTexture[i] = LoadTextureFromImage(resizeTexture);
					UnloadImage(resizeTexture);
				}
				//======================
			}
			else {
				unloadTextures();
				//======================
				half = 5;
				//---------
				WIDTH_SHAPES = static_cast<float>(ScreenManager::actualRes.WIDTH / 19.0f);	 //65
				HEIGHT_SHAPES = static_cast<float>(ScreenManager::actualRes.HEIGHT / 12.0f); //60
				//---------
				START_GRIDPOS = { static_cast<float>((ScreenManager::actualRes.WIDTH / half) - ((WIDTH_SHAPES / halfShapes) * size_X)),
				static_cast<float>(20) };
				//======================
				// GEMS LOAD RESIZE
				for (int i = 0; i < GEMS; i++) {
					resizeTexture = LoadImage(FormatText("res/assets/gridgems/gem%i.png", i));
					ImageResize(&resizeTexture, (int)WIDTH_SHAPES, (int)HEIGHT_SHAPES);
					whatTexture[i] = LoadTextureFromImage(resizeTexture);
					UnloadImage(resizeTexture);
				}
				//======================
			}
		}
		//------------
		void unloadTextures(){
			//---------------------
			for (int i = 0; i < GEMS; i++) { UnloadTexture(whatTexture[i]); }
			//---------------------
		}
		//------------
	}
}