#ifndef GRIDMANAGER_H
#define GRIDMANAGER_H

#include "raylib.h"

#define ALPHAGRID CLITERAL(Color){ 120, 20, 20, 205}

#define REDALPHA CLITERAL(Color)   { 230, 41,  55,  105 }
#define GREENALPHA CLITERAL(Color) { 0,   228, 48,  105 }
#define BLUEALPHA CLITERAL(Color)  { 0,   121, 241, 105 }
#define VIOLETALPHA CLITERAL(Color){ 135, 60,  190, 105 }
#define YELLOWALPHA CLITERAL(Color){ 253, 249,  0,  105 }

namespace Match3 {
	namespace GridHandle {
		//------------
		const int size_X = 8;
		const int size_Y = 8;
		const int colores = 5;
		//------------
		const int COLORS = 5;
		const int GEMS = 5;
		//------------
		const int TAMAUX = (int)(size_X*size_Y);
		//------------
		enum TYPE_SHAPE{
			red,
			green,
			blue,
			violet,
			yellow,
			black,
			nothing
		};
		//------------
		struct POS
		{
			int x;
			int y;
			bool ONTHATAUX;
		};
		//------------
		struct GRID{
			TYPE_SHAPE whatShape;
			POS position;
			Color colorShape;
			bool onSelec;
			bool canSelect;
			bool state;
			Rectangle gridShape;
			Rectangle shapes;
		};
		//------------
		extern bool gridBLOCKED;
		//------------
		extern GRID GAME_GRID[size_X][size_Y];
		extern GRID AUX_GRID[size_X][size_Y];
		//------------
		extern TYPE_SHAPE AUX_COLOR;
		//------------
		extern float SCALE_METTER;
		//------------
		extern bool SCAlE_METTER_MAX;
		//------------
		extern bool onUpdateGrid;
		//------------
		void initGridOnStart();
		//------------
		void drawGrid();
		//------------
		void updateGrid();
		//------------
		void checkGridPlayer();
		//------------
		void resizeGrid();
		//------------
		void loadTextures();
		//------------
		void unloadTextures();
		//------------
	}
}
#endif // !GRIDMANAGER_H