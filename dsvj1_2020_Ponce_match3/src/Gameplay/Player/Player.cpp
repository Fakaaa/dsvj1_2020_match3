#include "Player.h"

#include "raylib.h"

#include "Gameplay/gameHandle.h"
#include "Gameplay/GridManager/GridManager.h"
#include "Gameplay/Scene/GameScene.h"
#include "Gameplay/Enemy/Enemy.h"
#include "Menu/menuHandle.h"
#include "ScreenHandle/ScreenHandle.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;
using namespace ScreenManager;

namespace Match3 {
	namespace Player {
		//-----------------
			PLAYER pj;
		//-----------------
		Color mouseColor;
		bool swiping;
		bool onTouchMenu;
		int swipeCountShapes = 0;
		//-----------------
		int WIDTH_HEROES;
		int HEIGTH_HEROES;
		//-----------------
		float frameCounter = 0;
		float currentFrame= 0;
		float frameSpeed= 0;
		//-----------------
		float WIDTH_UXMETTER;
		float HEIGTH_UXMETTER;

		float HEIGTH_METTERBAR;
		//-----------------
		float makeAnimDamage;
		float currenTimeDamage;
		bool onAnimDamage;
		//-----------------
		float FULLL_HEALTH;
		//-----------------
		//--------
		float timeToMakeAnim;
		float actualTime;
		//-
		float timeToTransform;
		float posToReach;
		bool startTransform;
		float originalPos;
		//--------
		int auxRow;
		//-----------------
		bool onRestartAfterPlay = false;
		//-----------------
		float amountHelathRecover;
		//-----------------
		bool anticipateBoostX2;
		//-----------------
		float newPOSX;
		float newPOSY;
		//-----------------
		void initPlayer(){
			//-----------------------
			static float MTELEDURO = 100;
			//-----------------------
			onAnimDamage = false;
			makeAnimDamage = 0.4f;
			currenTimeDamage = 0;
			//-----------------------
			HideCursor();
			//-----------------------
			swipeCountShapes = 0;
			//-----------------------
			WIDTH_HEROES = (int)(actualRes.WIDTH / 6.0f);
			HEIGTH_HEROES = (int)(actualRes.HEIGHT / 4.0f);
			//-----------------------
			WIDTH_UXMETTER = (float)(actualRes.WIDTH / 17);
			HEIGTH_UXMETTER = (float)(actualRes.HEIGHT / 3.6f);
			HEIGTH_METTERBAR = (float)((actualRes.HEIGHT / 4.2f) - 5);
			FULLL_HEALTH = (int)((actualRes.WIDTH / 2.2f) + 10);
			//-----------------------
			pj.group.carry.pos = { (float)(actualRes.WIDTH / 1.7f),(float)(actualRes.HEIGHT / 3.0f) };
			originalPos = (float)(actualRes.WIDTH / 1.7f);
			//-----------------------
			if(!onRestartAfterPlay)
				loadPlayer();
			pj.group.carry.frameRec = {0.0f,0.0f, (float)(pj.group.carry.idle.width / 5),(float)(pj.group.carry.idle.height / 4) };
			//-----------------------
			frameSpeed = 6;
			//-----------------------
			pj.group.carry.posUXMetter = { static_cast<float>(actualRes.WIDTH / 2.2f),static_cast<float>(actualRes.HEIGHT / 5.5f) };
			//-----------------------
			pj.group.carry.atckMetterBar = { pj.group.carry.posUXMetter.x + (actualRes.WIDTH / 48),
				pj.group.carry.posUXMetter.y + (actualRes.HEIGHT / 30), WIDTH_UXMETTER / 3, 0 };
			//----------------------
			//pj.group.carry.atckMetterBar.height = 0;
			//-----------------------
			pj.group.posHealth = { (float)(actualRes.WIDTH / 2.2f),(float)(actualRes.HEIGHT / 1.1f) };
			//-----------------------
			pj.group.carry.metterBarAtck = 0;
			//-----------------------
			pj.group.healthBar.width = FULLL_HEALTH;
			pj.group.carry.doAtck = false;
			pj.group.health = (int)pj.group.healthBar.width;	//---------->	100%
			//======================================================
			timeToMakeAnim = 4.0f;
			actualTime = 0;
			//--------------------
			pj.group.carry.dps = (int)(actualRes.WIDTH / 40);
			//pj.group.carry.dps = MTELEDURO;		SI QUERES MATARLO DE UNA DESACTIVA ESTO
			//--------------------
			posToReach = (float)(ScreenManager::actualRes.WIDTH / 1.3f);

			pj.group.SHIELD_POS = { (float)(pj.group.carry.pos.x + ((pj.group.carry.idle.width / colsCarrySprite) / 2)),
									(float)(pj.group.carry.pos.y + ((pj.group.carry.idle.height / rowsCarrySprite) / 2))};

			pj.group.alive = true;
			pj.group.state = WHITE;
			//======================================================
			pj.group.carry.boostX2 = false;
			anticipateBoostX2 = false;
			pj.group.carry.posTextX2BOOST = {(float)((pj.group.carry.posUXMetter.x)+(pj.group.carry.atckMetterUX.width /2)),
											(float)((pj.group.carry.posUXMetter.y) + (pj.group.carry.atckMetterUX.height )) };
			pj.group.carry.X2BOOST = VIOLET;
			//======================================================
			if (MenuHandle::onMenu)
				GameHandle::makingTransition = false;
		}
		//-----------------
		void inputsPlayer(){
			//--------
			if (IsMouseButtonDown(MOUSE_LEFT_BUTTON)) { mouseColor = GREEN; swiping = true; onTouchMenu = true; }
			else { mouseColor = WHITE; swiping = false; GridHandle::onUpdateGrid = true; onTouchMenu = false; }
			//--------
		}
		//-----------------
		void deinitPlayer(){
			//-------------------
			unloadPlayer();
			//-------------------
		}
		//-----------------
		void calcFrameAnim(Rectangle & frameRec, Texture2D& spriteCalc,const int & frameCols, const int & frameRows){
			//------------------------------------------
			frameCounter++;

			if (frameCounter >= (ScreenManager::maxFPS / frameSpeed)) {
				frameCounter = 0;
				currentFrame++;

				if (auxRow >= frameRows) {
					auxRow = 0;
					currentFrame = 0;
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
					frameRec.y = (float)auxRow*(float)((spriteCalc.height / frameRows));
				}

				if (frameRec.x <= (frameCols * WIDTH_HEROES)) {
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
				}
				else {
					auxRow++;
					currentFrame = 0;
					frameRec.x = (float)currentFrame*(float)(spriteCalc.width / frameCols);
					frameRec.y = (float)auxRow*(float)((spriteCalc.height / frameRows));
				}
			}
			if (frameSpeed >= MAX_FRAME_SPEED) frameSpeed = MAX_FRAME_SPEED;
			else if (frameSpeed <= MIN_FRAME_SPEED) frameSpeed = MIN_FRAME_SPEED;
			//------------------------------------------
		}
		//-----------------
		static void calcDPSCarry() {
			//------------------
			if (pj.group.carry.boostX2 && pj.group.carry.doAtck) {
				pj.group.carry.dps = (int)((actualRes.WIDTH / 40) * 2);
				pj.group.carry.boostX2 = false;
			}
			//------------------
		}
		//-----------------
		void usePurpleGems(int& maxX, int& maxY) {
			//----------------
			if (swipeCountShapes >= 3 && swipeCountShapes <= (maxX * maxY)) {
				pj.group.carry.boostX2 = true;
			}
			//----------------
		}
		//-----------------
		void useYellowGems(int& maxX, int& maxY) {
			//-------------
			static float restAmountTime = 0.4f;
			//-------------
			if (swipeCountShapes >= 3 && swipeCountShapes <= (maxX * maxY)) {
				if ((restAmountTime * swipeCountShapes) < EnemyHandle::timeBettewnAtacks && EnemyHandle::actualTimeCharged >= 0) {
					EnemyHandle::enemyMaze.chargerBar.width -= (int)((ScreenManager::actualRes.WIDTH / 120) * swipeCountShapes);
					EnemyHandle::actualTimeCharged -= (float)(restAmountTime * swipeCountShapes);
				}

				pj.group.debuffEnemy = true;
			}
			//-------------
		}
		//-----------------
		void shieldTeam(int& sizeX, int& sizeY){
			//--------------------
			if (swipeCountShapes >= 3 && swipeCountShapes <= (sizeX * sizeY)) {
				pj.group.shieldOn = true;
			}
			//--------------------
		}
		//-----------------
		static void teamDead() {
			//-------------
			static bool makeSoundDead = false;
			//-------------
			if (pj.group.healthBar.width <= 20) {

				if (!makeSoundDead) {
					SoundsHandle::stopSound(SoundsHandle::playerDeath.sfx);
					SoundsHandle::playSound(SoundsHandle::playerDeath.sfx);
					makeSoundDead = false;
				}
				if (pj.group.state.a >= 0)
					pj.group.state.a -= (150 * GetFrameTime());
				if(pj.group.state.a <= 0) {
					pj.group.state.a = 0;
					GameHandle::makingTransition = true;
				}
				frameSpeed = 0; 
				pj.group.alive = false;
			}
			//-------------
		}
		//-----------------
		void healTeam() {
			//-------------
			if (swipeCountShapes >= 3 && swipeCountShapes <= 6) {
				amountHelathRecover = (float)((actualRes.WIDTH / 120) * swipeCountShapes);

				if ((pj.group.health + amountHelathRecover) <= FULLL_HEALTH)
					pj.group.health += (int)amountHelathRecover;
				if ((pj.group.health + amountHelathRecover) >= FULLL_HEALTH) {
					pj.group.health = FULLL_HEALTH;
				}
				pj.group.healthBar.width = pj.group.health;
			}
			else if (swipeCountShapes >= 6 && swipeCountShapes <= 10) {
				amountHelathRecover = (float)((actualRes.WIDTH / 120) * swipeCountShapes);

				if ((pj.group.health + amountHelathRecover) <= FULLL_HEALTH)
					pj.group.health += (int)amountHelathRecover;
				if ((pj.group.health + amountHelathRecover) >= FULLL_HEALTH) {
					pj.group.health = FULLL_HEALTH;
				}
				pj.group.healthBar.width = pj.group.health;
			}
			//-------------
		}
		//-----------------
		void getDamageOnTeam(){
			//------------------------
			if (EnemyHandle::enemyMaze.onAtack && EnemyHandle::actualTimeCharged >= EnemyHandle::timeBettewnAtacks) {

				if (currenTimeDamage <= makeAnimDamage && !pj.group.shieldOn) {
					currenTimeDamage += GetFrameTime();
				}
				else if (currenTimeDamage > makeAnimDamage) {
					EnemyHandle::enemyMaze.onAtack = false;
					EnemyHandle::startTranslate = false;
					EnemyHandle::enemyMaze.chargerBar.width = 0;
					EnemyHandle::actualTimeCharged = 0;
					EnemyHandle::ANIME_STATE = 1;
					EnemyHandle::ON_ATTACK = false;
					GridHandle::gridBLOCKED = false;
					currenTimeDamage = 0;
				}

				frameSpeed = 0;
				if (!pj.group.shieldOn) {
					pj.group.health -= EnemyHandle::enemyMaze.damageMaded;
				}
				else if (pj.group.shieldOn) {
					EnemyHandle::enemyMaze.onAtack = false;
					EnemyHandle::enemyMaze.onAtack = false;
					EnemyHandle::startTranslate = false;
					EnemyHandle::enemyMaze.chargerBar.width = 0;
					EnemyHandle::actualTimeCharged = 0;
					EnemyHandle::ANIME_STATE = 1;
					EnemyHandle::ON_ATTACK = false;
					GridHandle::gridBLOCKED = false;
					currenTimeDamage = 0;
					pj.group.shieldOn = false;
					return;
				}
				//Transition on health texture
				if (pj.group.healthBar.width >= 0)
					pj.group.healthBar.width = pj.group.health;
				else
					pj.group.healthBar.width = 0;
			}
			//------------------------
		}
		//-----------------
		static void updateShieldPos() {
			//---------------------
			pj.group.SHIELD_POS = { (float)(pj.group.carry.pos.x + ((pj.group.carry.idle.width / colsCarrySprite) / 2)),
									(float)(pj.group.carry.pos.y + ((pj.group.carry.idle.height / rowsCarrySprite) / 2)) };
			//---------------------
		}
		//-----------------
		void drawPj(){
			//---------------------
			static int sizeTextX2 = (int)(actualRes.WIDTH / 40);
			//---------------------
			//--------
			if (!pj.group.carry.doAtck) {
				if(!pj.group.carry.boostX2)
					DrawTextureRec(pj.group.carry.idle,pj.group.carry.frameRec, pj.group.carry.pos, pj.group.state);
				else if(pj.group.carry.boostX2)
					DrawTextureRec(pj.group.carry.idle,pj.group.carry.frameRec, pj.group.carry.pos, PURPLE);
			}
			else {
				if (!pj.group.carry.boostX2)
					DrawTextureRec(pj.group.carry.atck, pj.group.carry.frameRec, pj.group.carry.pos, pj.group.state);
				else if (pj.group.carry.boostX2)
					DrawTextureRec(pj.group.carry.atck, pj.group.carry.frameRec, pj.group.carry.pos, PURPLE);
			}
			//--------
			if (pj.group.shieldOn) {
				DrawCircleV(pj.group.SHIELD_POS, 100, BLUEALPHA);
			}
			if (anticipateBoostX2) {
				DrawText("X2 DPS", (int)pj.group.carry.posTextX2BOOST.x, (int)pj.group.carry.posTextX2BOOST.y, sizeTextX2, VIOLET);
			}
			if(!pj.group.carry.boostX2){
				anticipateBoostX2 = false;
			}
			//--------
			DrawRectangleRec(pj.group.carry.atckMetterBar, RED);
			//--------
			DrawTextureEx(pj.group.carry.atckMetterUX, pj.group.carry.posUXMetter, 0.0f, 1.0f, WHITE);
			//--------
			DrawTextureEx(pj.group.healthBarUX, pj.group.posHealth, 0.0f, 1.0f, WHITE);
			//--------
			DrawTextureEx(pj.group.healthBar, pj.group.posHealth, 0.0f, 1.0f, GREEN);
			//--------
		}
		//-----------------
		void drawCursor(){
			//--------------
			DrawTextureEx(pj.cursorSkin, pj.POS_MOUSE, 0.0f, 1.0f, YELLOW);
			//--------------
		}
		//-----------------
		void updatePlayerMouse(){
			//--------
			pj.POS_MOUSE = GetMousePosition();
			//--------
			if(!SceneHandle::onPause)updatePlayerSprite();
			//--------
		}
		//-----------------
		static void doTrasnform() {
			//------------------------
			if (pj.group.carry.pos.x <= posToReach && startTransform)
				pj.group.carry.pos.x += 4;
			if (pj.group.carry.pos.x >= posToReach && startTransform) {
				Player::pj.group.carry.doAtck = true;
			}
			if (pj.group.carry.pos.x >= originalPos && !startTransform)
				pj.group.carry.pos.x -= 4;
			//------------------------
		}
		//-----------------
		static void calcTimeTransAndReset() {
			//----------------------------
			if (actualTime <= timeToMakeAnim && GridHandle::SCAlE_METTER_MAX)
				actualTime += GetFrameTime();
			if (actualTime > timeToMakeAnim) {
				GridHandle::SCAlE_METTER_MAX = false;
				pj.group.carry.atckMetterBar.height = 0;
				GridHandle::SCALE_METTER = 0;
				actualTime = 0;
				pj.group.carry.doAtck = false;
				startTransform = false;
			}
			//----------------------------
			if (actualTime >= 1.5) {
				startTransform = true;

			}
			else if (actualTime >= 0.5 && !startTransform) {
				currentFrame = 0;
				auxRow = 0;
				EnemyHandle::ATACKED = true;
			}
			//----------------------------
		}
		//-----------------
		static void changeAnimSprite() {
			//------------------
			if (!pj.group.carry.doAtck) {
				calcFrameAnim(pj.group.carry.frameRec, pj.group.carry.idle, colsCarrySprite, rowsCarrySprite);
			}
			else {
				calcFrameAnim(pj.group.carry.frameRec, pj.group.carry.atck, colsCarrySprite, rowsCarrySprite);
			}
			//------------------
		}
		//-----------------
		void updatePlayerSprite(){
			//-----------
			getDamageOnTeam();
			//-----------
			calcTimeTransAndReset();
			//-----------
			doTrasnform();
			//-----------
			updateShieldPos();
			//-----------
			changeAnimSprite();
			//-----------
			teamDead();
			//-----------
			calcDPSCarry();
			//-----------
		}
		//-----------------
		void loadPlayer(){
			//---------
			Image resize;
			//---------
			if (!onResizeWindow) {
				//---------
				resize = LoadImage("res/assets/player/cursor/glove.png");
				ImageResize(&resize, (int)(actualRes.WIDTH / 25), (int)(actualRes.HEIGHT / 14.2f));
				pj.cursorSkin = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/carry/carryIdle.png");
				ImageResize(&resize, (int)WIDTH_HEROES * colsCarrySprite, (int)HEIGTH_HEROES * rowsCarrySprite);
				pj.group.carry.idle = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/carry/carryAttck.png");
				ImageResize(&resize, (int)WIDTH_HEROES* colsCarrySprite, (int)HEIGTH_HEROES* rowsCarrySprite);
				pj.group.carry.atck = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/atackMetterUX.png");
				ImageResize(&resize, (int)WIDTH_UXMETTER , (int)HEIGTH_UXMETTER);
				pj.group.carry.atckMetterUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/healthTeam.png");
				ImageResize(&resize, (int)(FULLL_HEALTH), (int)((actualRes.HEIGHT / 25)));
				pj.group.healthBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/healthTeamUX.png");
				ImageResize(&resize, (int)((actualRes.WIDTH / 2.2f) + 4), (int)((actualRes.HEIGHT / 25)- 4));
				pj.group.healthBarUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
			}
			else {
				//---------
				unloadPlayer();
				//---------
				WIDTH_HEROES = (int)(actualRes.WIDTH / 6.0f);
				HEIGTH_HEROES = (int)(actualRes.HEIGHT / 4.0f);
				WIDTH_UXMETTER = (float)(actualRes.WIDTH / 17);
				HEIGTH_UXMETTER = (float)(actualRes.HEIGHT / 3.6f);
				HEIGTH_METTERBAR = (float)((actualRes.HEIGHT / 4.2f) - 5);
				FULLL_HEALTH = (float)((actualRes.WIDTH / 2.2f) + 10);
				newPOSX = (float)((actualRes.WIDTH / 2.2f) + (actualRes.WIDTH / 48));
				newPOSY = (float)((actualRes.HEIGHT / 5.5f) + (actualRes.HEIGHT / 30));
				//---------
				pj.group.carry.pos = { (float)(actualRes.WIDTH / 1.7f),(float)(actualRes.HEIGHT / 3.0f) };
				pj.group.posHealth = { (float)(actualRes.WIDTH / 2.2f),(float)(actualRes.HEIGHT / 1.1f) };
				originalPos = (float)(actualRes.WIDTH / 1.7f);
				//---------
				pj.group.carry.metterBarAtck = 0;
				pj.group.carry.posUXMetter = { static_cast<float>(actualRes.WIDTH / 2.2f),static_cast<float>(actualRes.HEIGHT / 5.5f) };
				posToReach = (float)(ScreenManager::actualRes.WIDTH / 1.3f);
				//---------
				resize = LoadImage("res/assets/player/cursor/glove.png");
				ImageResize(&resize, (int)(actualRes.WIDTH / 25), (int)(actualRes.HEIGHT / 14.2f));
				pj.cursorSkin = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/carry/carryIdle.png");
				ImageResize(&resize, (int)WIDTH_HEROES * colsCarrySprite, (int)HEIGTH_HEROES * rowsCarrySprite);
				pj.group.carry.idle = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/carry/carryAttck.png");
				ImageResize(&resize, (int)WIDTH_HEROES* colsCarrySprite, (int)HEIGTH_HEROES* rowsCarrySprite);
				pj.group.carry.atck = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/atackMetterUX.png");
				ImageResize(&resize, (int)WIDTH_UXMETTER, (int)HEIGTH_UXMETTER);
				pj.group.carry.atckMetterUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/healthTeam.png");
				ImageResize(&resize, (int)(FULLL_HEALTH), (int)((actualRes.HEIGHT / 25)));
				pj.group.healthBar = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				resize = LoadImage("res/assets/player/healthTeamUX.png");
				ImageResize(&resize, (int)((actualRes.WIDTH / 2.2f) + 4), (int)((actualRes.HEIGHT / 25) - 4));
				pj.group.healthBarUX = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//---------
				pj.group.carry.atckMetterBar = { newPOSX, newPOSY, WIDTH_UXMETTER / 3, 0 };
				pj.group.carry.frameRec = { 0.0f,0.0f, (float)(pj.group.carry.idle.width / 5),(float)(pj.group.carry.idle.height / 4) };
				//-------------
				pj.group.carry.dps = (int)(actualRes.WIDTH / 40);

				pj.group.SHIELD_POS = { (float)(pj.group.carry.pos.x + (pj.group.carry.idle.width / 2)),
								(float)(pj.group.carry.pos.y + (pj.group.carry.idle.height / 2)) };
				//pj.group.carry.dps = 100;	//SI SETEAS PARA MATARLO DE UNA TAMBIEN DESCOMENTA ESTO
				//pj.group.carry.atckMetterBar.height = 0;
				pj.group.carry.metterBarAtck = 0;
			}
			onRestartAfterPlay = true;
		}
		//-----------------
		void unloadPlayer(){
			//------------
			UnloadTexture(pj.cursorSkin);
			//------------
			UnloadTexture(pj.group.carry.idle);
			//------------
			UnloadTexture(pj.group.carry.atckMetterUX);
			//------------
			UnloadTexture(pj.group.healthBar);
			//------------
			UnloadTexture(pj.group.healthBarUX);
			//------------
			UnloadTexture(pj.group.carry.atck);
			//------------
		}
		//-----------------
	}
}