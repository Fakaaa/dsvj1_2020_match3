#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

#define MAX_FRAME_SPEED 20
#define MIN_FRAME_SPEED 6

namespace Match3 {
	namespace Player {
		//-----------------
		const int ANIME_STATE = 2;
		const int SWIPEMAX = 7;
		//-----------------
		const int colsCarrySprite = 5;
		const int rowsCarrySprite = 4;
		//-----------------
		struct DPS
		{
			Texture2D idle;
			Texture2D atck;
			Rectangle frameRec;
			Vector2 pos;
			bool boostX2;
			bool doAtck;

			Texture2D atckMetterUX;
			Rectangle atckMetterBar;
			Vector2 posUXMetter;
			Vector2 posTextX2BOOST;
			Color X2BOOST;
			float metterBarAtck;

			int dps;
		};
		struct SUPPORT {
			Texture2D IDLE;
			Texture2D BOOST;
			Rectangle frameRec;
			Vector2 pos;

			bool debuffEnemy;
			bool healTeam;
			bool shieldTeam;
		};
		//-----------------
		struct TEAM
		{
			DPS carry;
			SUPPORT mage;

			bool debuffEnemy;
			bool shieldOn;

			Vector2 SHIELD_POS;

			Texture2D healthBarUX;
			Texture2D healthBar;
			Vector2	posHealth;
			bool alive;
			Color state;
			int health;
		};
		//-----------------
		struct PLAYER{
			Vector2 POS_MOUSE;
			Texture2D cursorSkin;
			TEAM group;
		};
		//-----------------
		extern PLAYER pj;
		//-----------------
		extern float HEIGTH_METTERBAR;
		//-----------------
		extern bool swiping;
		extern bool onTouchMenu;
		//-----------------
		extern int swipeCountShapes;
		extern float amountHelathRecover;
		extern bool anticipateBoostX2;
		//-----------------
		void initPlayer();
		//-----------------
		void inputsPlayer();
		//-----------------
		void deinitPlayer();
		//-----------------
		void calcFrameAnim(Rectangle& frameRec, Texture2D& spriteCalc, const int & frameCols, const int & frameRows);
		//-----------------
		void usePurpleGems(int& maxX, int& maxY);
		//-----------------
		void useYellowGems(int& maxX, int& maxY);
		//-----------------
		void shieldTeam(int& sizeX, int& sizeY);
		//-----------------
		void healTeam();
		//-----------------
		void getDamageOnTeam();
		//-----------------
		void drawPj();
		//-----------------
		void drawCursor();
		//-----------------
		void updatePlayerMouse();
		//-----------------
		void updatePlayerSprite();
		//-----------------
		void loadPlayer();
		//-----------------
		void unloadPlayer();
		//-----------------
	}
}
#endif // !PLAYER_H