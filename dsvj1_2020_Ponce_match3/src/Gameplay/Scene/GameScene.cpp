#include "GameScene.h"

#include "ScreenHandle/ScreenHandle.h"
#include "Gameplay/Player/Player.h"
#include "Menu/menuHandle.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;

namespace Match3 {
	namespace SceneHandle {
		//======================================
		BACKGROUND sceneBG;
		float WIDTH;
		float HEIGTH;
		bool onPause;
		int iterarBgs;
		bool firstInit = false;
		//======================================
		void init(){
			//------------
			iterarBgs = GetRandomValue(1,10);
			//------------
			WIDTH = static_cast<float>((ScreenManager::actualRes.WIDTH / 3) - ((ScreenManager::actualRes.WIDTH / 6.2f) / 2));
			HEIGTH = static_cast<float>((ScreenManager::actualRes.HEIGHT / 4) - ((ScreenManager::actualRes.HEIGHT / 3.2f) / 2));
			sceneBG.posBG = { 0.0f , 0.0f};
			sceneBG.pauseAlpha = { sceneBG.posBG.x, sceneBG.posBG.y,
				(float)(ScreenManager::actualRes.WIDTH) , (float)(ScreenManager::actualRes.HEIGHT) };
			//------------
			if(!firstInit)
				load();

			sceneBG.pause.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH) - (sceneBG.pause.BTN->width / 1.5f),
				static_cast<float> (ScreenManager::actualRes.HEIGHT / 45)};
			//-----------------------
			sceneBG.resume.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH / 1.8f) - (sceneBG.resume.BTN->width / 1.4f),
				static_cast<float> (ScreenManager::actualRes.HEIGHT / 2) };
			//---
			sceneBG.backMenu.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH / 1.8f) - (sceneBG.backMenu.BTN->width / 1.4f),
				static_cast<float> (ScreenManager::actualRes.HEIGHT / 1.4f) };
			//-----------------------
			sceneBG.posPauseUX = { static_cast<float>((ScreenManager::actualRes.WIDTH / 4)), static_cast<float> (ScreenManager::actualRes.HEIGHT / 7.5f)};
			//-----------------------
			sceneBG.pause.state = false;
			sceneBG.pause.in = false;
			//--
			sceneBG.resume.state = false;
			sceneBG.resume.in = false;
			//--
			sceneBG.backMenu.state = false;
			sceneBG.backMenu.in = false;
			//------------
			onPause = false;
			//-----
			sceneBG.pause.collider = { sceneBG.pause.posText.x + 100, sceneBG.pause.posText.y ,
				(float)(WIDTH / 2.8f), HEIGTH};
			//-----
			sceneBG.resume.collider = { sceneBG.resume.posText.x, sceneBG.resume.posText.y ,
				(float)(WIDTH), HEIGTH };
			//-----
			sceneBG.backMenu.collider = { sceneBG.backMenu.posText.x, sceneBG.backMenu.posText.y ,
				(float)(WIDTH), HEIGTH };
			//-----
		}
		//======================================
		void drawBg(){
			//---------------
			DrawTextureEx(sceneBG.bg[iterarBgs], sceneBG.posBG, 0.0f, 1.0f, WHITE);
			//---------------
		}
		//======================================
		void drawPause(){
			//------------------------
			if (!sceneBG.pause.in) {
				DrawTextureEx(sceneBG.pause.BTN[0], sceneBG.pause.posText, 0.0f, 1.0f, WHITE);
			}
			else {
				DrawTextureEx(sceneBG.pause.BTN[1], sceneBG.pause.posText, 0.0f, 1.0f, WHITE);
			}
			if (sceneBG.pause.state == true) {
				DrawTextureEx(sceneBG.pause.BTN[2], sceneBG.pause.posText, 0.0f, 1.0f, WHITE);
				sceneBG.pause.state = false;
			}
			if (onPause) {
				//------------------------
				DrawTextureEx(sceneBG.pauseMenuUI, sceneBG.posPauseUX, 0.0f, 1.0f, WHITE);
				//------------------------
				if (!sceneBG.resume.in) {
					DrawTextureEx(sceneBG.resume.BTN[0], sceneBG.resume.posText, 0.0f, 1.0f, WHITE);
				}
				else {
					DrawTextureEx(sceneBG.resume.BTN[1], sceneBG.resume.posText, 0.0f, 1.0f, WHITE);
				}
				if (!sceneBG.backMenu.in) {
					DrawTextureEx(sceneBG.backMenu.BTN[0], sceneBG.backMenu.posText, 0.0f, 1.0f, WHITE);
				}
				else {
					DrawTextureEx(sceneBG.backMenu.BTN[1], sceneBG.backMenu.posText, 0.0f, 1.0f, WHITE);
				}
				//------------------------
				if (sceneBG.resume.state == true) {
					DrawTextureEx(sceneBG.resume.BTN[2], sceneBG.resume.posText, 0.0f, 1.0f, WHITE);
					sceneBG.resume.state = false;
				}
				if (sceneBG.backMenu.state == true) {
					DrawTextureEx(sceneBG.backMenu.BTN[2], sceneBG.backMenu.posText, 0.0f, 1.0f, WHITE);
					sceneBG.backMenu.state = false;
				}
				//------------------------
			}
			//------------------------
		}
		//======================================
		void deinit(){
			//---------
			unload();
			//---------
		}
		//======================================
		void load(){
			//-----------------------------
			Image resize;
			//-----------------------------
			if (!ScreenManager::onResizeWindow) {
				//=========================================================
				for (int i = 1; i <= BACKGROUNDS; i++){
					resize = LoadImage(FormatText("res/assets/scene/battleback%i.png",i));
					ImageResize(&resize, (int)ScreenManager::actualRes.WIDTH, (int)ScreenManager::actualRes.HEIGHT);
					sceneBG.bg[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/pause%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.pause.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/menu/resume%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.resume.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/menu/menu%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.backMenu.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				resize = LoadImage("res/assets/menu/pauseMenu.png");
				ImageResize(&resize, (int)(ScreenManager::actualRes.WIDTH / 2.0f),
					(int)(ScreenManager::actualRes.HEIGHT / 1.1f));
				sceneBG.pauseMenuUI = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=========================================================
			}
			else {
				//-------------
				unload();
				//-------------
				WIDTH = static_cast<float>((ScreenManager::actualRes.WIDTH / 3) - ((ScreenManager::actualRes.WIDTH / 6.2f)/2));
				HEIGTH = static_cast<float>((ScreenManager::actualRes.HEIGHT / 4) - ((ScreenManager::actualRes.HEIGHT / 3.2f) / 2));
				//=========================================================
				for (int i = 1; i <= BACKGROUNDS; i++) {
					resize = LoadImage(FormatText("res/assets/scene/battleback%i.png",i));
					ImageResize(&resize, (int)ScreenManager::actualRes.WIDTH, (int)ScreenManager::actualRes.HEIGHT);
					sceneBG.bg[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/pause%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.pause.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/menu/resume%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.resume.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				for (int i = 0; i < 3; i++) {
					resize = LoadImage(FormatText("res/assets/menu/menu%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					sceneBG.backMenu.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//-------------
				resize = LoadImage("res/assets/menu/pauseMenu.png");
				ImageResize(&resize, (int)(ScreenManager::actualRes.WIDTH / 2.0f),
					(int)(ScreenManager::actualRes.HEIGHT / 1.1f));
				sceneBG.pauseMenuUI = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=========================================================
				sceneBG.posBG = { 0.0f , 0.0f };
				sceneBG.pauseAlpha = { sceneBG.posBG.x, sceneBG.posBG.y,
					(float)(ScreenManager::actualRes.WIDTH) ,(float)(ScreenManager::actualRes.HEIGHT) };
				//------------------
				sceneBG.pause.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH) - (sceneBG.pause.BTN->width / 1.5f),
				static_cast<float> (ScreenManager::actualRes.HEIGHT / 45) };
				//-----
				sceneBG.resume.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH / 1.8f) - (sceneBG.resume.BTN->width / 1.4f),
				static_cast<float> (ScreenManager::actualRes.HEIGHT / 2) };
				//---
				sceneBG.backMenu.posText = { static_cast<float>(ScreenManager::actualRes.WIDTH / 1.8f) - (sceneBG.backMenu.BTN->width / 1.4f),
					static_cast<float> (ScreenManager::actualRes.HEIGHT / 1.4f) };
				//-----------------------
				sceneBG.posPauseUX = { static_cast<float>((ScreenManager::actualRes.WIDTH / 4)), static_cast<float> (ScreenManager::actualRes.HEIGHT / 7.5f) };
				//-----------------------
				static float auxPuaseColliderX = (float)((ScreenManager::actualRes.WIDTH) - (sceneBG.pause.BTN->width / 1.5f)
					+ (ScreenManager::actualRes.WIDTH / 12.4f));
				static float auxPuaseColliderY = (float)(ScreenManager::actualRes.HEIGHT / 45);
				//-----------------------
				sceneBG.pause.collider = { auxPuaseColliderX, auxPuaseColliderY ,(float)(WIDTH / 2.8f), HEIGTH };
				//-----
				sceneBG.resume.collider = { sceneBG.resume.posText.x, sceneBG.resume.posText.y ,
					(float)(WIDTH), HEIGTH };
				//-----
				sceneBG.backMenu.collider = { sceneBG.backMenu.posText.x, sceneBG.backMenu.posText.y ,
					(float)(WIDTH), HEIGTH };
				//-----
			}
			//-----------------------------
			firstInit = true;
		}
		//======================================
		void unload(){
			//----------------
			for (int i = 1; i <= BACKGROUNDS; i++) { UnloadTexture(sceneBG.bg[i]); }
			//----------------
			for (int i = 0; i < 3; i++) { UnloadTexture(sceneBG.pause.BTN[i]); }
			//----------------
			for (int i = 0; i < 3; i++) { UnloadTexture(sceneBG.resume.BTN[i]); }
			//----------------
			for (int i = 0; i < 3; i++) { UnloadTexture(sceneBG.backMenu.BTN[i]); }
			//----------------
		}
		//======================================
		void update(){
			//---------
			if (!onPause) {
				//---------
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, sceneBG.pause.collider)) {
					sceneBG.pause.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					sceneBG.pause.in = false;
				}
				//---------
				if (sceneBG.pause.in && Player::onTouchMenu && !sceneBG.pause.state) {
					sceneBG.pause.state = true;
					onPause = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//---------
			}
			else {
				//---------
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, sceneBG.resume.collider)) {
					sceneBG.resume.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					sceneBG.resume.in = false;
				}
				//---------
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, sceneBG.backMenu.collider)) {
					sceneBG.backMenu.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					sceneBG.backMenu.in = false;
				}
				//---------
				if (sceneBG.resume.in && Player::onTouchMenu && !sceneBG.resume.state) {
					sceneBG.resume.state = true;
					onPause = false;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				if (sceneBG.backMenu.in && Player::onTouchMenu && !sceneBG.backMenu.state) {
					sceneBG.backMenu.state = true;
					MenuHandle::onMenu = !MenuHandle::onMenu;
					onPause = false;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//---------
			}
		}
		//======================================
	}
}