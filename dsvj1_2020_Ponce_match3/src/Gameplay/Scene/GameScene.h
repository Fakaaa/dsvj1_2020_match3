#ifndef GAMESCENE_H
#define GAMESCENE_H

#include "raylib.h"
#include "Menu/Buttons/buttons.h"

using namespace Match3;
using namespace ButtonsHandle;

#define PAUSE CLITERAL(Color){ 0, 0, 0, 125}

namespace Match3 {
	namespace SceneHandle {
		//---------------------
		const int BACKGROUNDS = 10;
		//---------------------
		struct BACKGROUND {
			Texture2D bg[BACKGROUNDS];
			Texture2D scorePlayer;
			Vector2 posBG;

			Texture2D pauseMenuUI;
			Vector2 posPauseUX;
			BUTTONS pause;
			BUTTONS resume;
			BUTTONS backMenu;

			Rectangle pauseAlpha;
		};
		//---------------------
		extern BACKGROUND sceneBG;
		//---------------------
		extern bool onPause;
		//---------------------
		void init();
		//---------------------
		void drawBg();
		//---------------------
		void drawPause();
		//---------------------
		void deinit();
		//---------------------
		void load();
		//---------------------
		void unload();
		//---------------------
		void update();
		//---------------------
	}
}
#endif