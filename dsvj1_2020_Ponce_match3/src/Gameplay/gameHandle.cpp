#include "gameHandle.h"

#include "GridManager/GridManager.h"
#include "ScreenHandle/ScreenHandle.h"
#include "Player/Player.h"
#include "Scene/GameScene.h"
#include "Enemy/Enemy.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;

#define TRANSITION_ON CLITERAL(Color){0,0,0,255}
#define TRANSITION_OFF CLITERAL(Color){0,0,0,0}

namespace Match3 {
	namespace GameHandle {
		//-----------
		Rectangle transitionRec;
		Color colorTransition;
		bool makingTransition;
		bool fadeInDone = false;

		int timesPlayerWin;

		float actualTime;
		float timeToTransitionIn;
		float timeToTransitionOut;

		bool inPauseScreen = false;
		bool toMenuAfterDefeat = false;
		//-----------
		void makeTransitionToNext(){
			//----------------
			if (makingTransition && actualTime <= timeToTransitionIn && !fadeInDone && Player::pj.group.alive) {
				//-----
				actualTime += GetFrameTime();
				//-----

				if(colorTransition.a <= 255)
					colorTransition.a++;
				if(colorTransition.a >= 255){
					colorTransition = TRANSITION_ON;
					restartAfetrExit();
					fadeInDone = true;
					actualTime = 0;
					timesPlayerWin++;
					EnemyHandle::enemyMaze.damageMaded += 1;
				}	
			}
			if (makingTransition && actualTime <= timeToTransitionOut && fadeInDone && Player::pj.group.alive) {
				//-----
				actualTime += GetFrameTime();
				//-----
				if (colorTransition.a >= 0) {
					colorTransition.a--;
				}
				if (colorTransition.a <= 0) {
					colorTransition = TRANSITION_OFF;
					fadeInDone = false;
					makingTransition = false;
				}
			}
			//==============================================================
		}
		//-----------
		void makeTransWhenDeadP1(){
			//----------------------------------
			if (makingTransition && actualTime <= timeToTransitionOut && !fadeInDone && !Player::pj.group.alive) {
				//-----
				actualTime += GetFrameTime();
				//-----
				if (colorTransition.a <= 255)
					colorTransition.a++;
				if (colorTransition.a >= 255) {
					colorTransition = TRANSITION_ON;
					fadeInDone = true;
					toMenuAfterDefeat = true;
					actualTime = 0;
				}
			}
			//----------------------------------
		}
		//-----------
		void makeTransWhenDeadP2(){
			//---------------------------
			if (makingTransition && actualTime <= timeToTransitionOut && fadeInDone && !Player::pj.group.alive) {
				//-----
				actualTime += GetFrameTime();
				//-----
				if (colorTransition.a >= 0) {
					colorTransition.a--;
				}
				if (colorTransition.a <= 0) {
					colorTransition = TRANSITION_OFF;
					timesPlayerWin = 0;
					EnemyHandle::enemyMaze.damageMaded = 2;
					fadeInDone = false;
					makingTransition = false;
					toMenuAfterDefeat = false;
				}
			}
			//---------------------------
		}
		//-----------
		void initGameThings() {
			//-------
			transitionRec = {0.0f, 0.0f, (float)(ScreenManager::actualRes.WIDTH),(float)(ScreenManager::actualRes.HEIGHT) };
			makingTransition = false;
			toMenuAfterDefeat = false;
			fadeInDone = false;
			colorTransition = TRANSITION_OFF;
			timeToTransitionIn = 10.0f;
			timeToTransitionOut = 10.0f;
			actualTime = 0;
			timesPlayerWin = 0;
			//-------
			inPauseScreen = false;
			//-------
			Player::initPlayer();
			//-------
			EnemyHandle::init();
			//-------
			GridHandle::initGridOnStart();
			//-------
			SceneHandle::init();
			//-------
		}
		//-----------
		void deinitGameThings() {
			//-------
			Player::deinitPlayer();
			//-------
			EnemyHandle::deinit();
			//-------
			GridHandle::unloadTextures();
			//-------
			SceneHandle::deinit();
			//-------
		}
		//-----------
		void updateGame() {
			//----------------------
			if (!makingTransition) {
				//-------
				SceneHandle::update();
				//-------
				EnemyHandle::upadate();
				//-------
				Player::updatePlayerMouse();
				//-------
				if (!SceneHandle::onPause && !EnemyHandle::ON_ATTACK)
					GridHandle::checkGridPlayer();
				else if (EnemyHandle::ON_ATTACK)
					GridHandle::gridBLOCKED = true;
				//-------
				if (SceneHandle::onPause)
					inPauseScreen = true;
				else
					inPauseScreen = false;
				//-------
			}
			else {
				//-----
				makeTransitionToNext();
				//-----
				makeTransWhenDeadP1();
				//-----
			}
			//----------------------
		}
		//-----------
		void inputsGame(){
			//-------
			Player::inputsPlayer();
			//-------
		}
		//-----------
		void drawGame() {
			//-------
			SceneHandle::drawBg();
			//-------
			GridHandle::drawGrid();
			//-------
			if (EnemyHandle::ON_ATTACK){
				//-------
				Player::drawPj();
				//-------
				EnemyHandle::drawEnemy();
				//-------
			}
			else if (Player::pj.group.carry.doAtck) {
				//-------
				EnemyHandle::drawEnemy();
				//-------
				Player::drawPj();
				//-------
			}
			else {
				//-------
				Player::drawPj();
				//-------
				EnemyHandle::drawEnemy();
				//-------
			}
			if (SceneHandle::onPause) DrawRectangleRec(SceneHandle::sceneBG.pauseAlpha, PAUSE);
			//-------
			SceneHandle::drawPause();
			//-------
			Player::drawCursor();
			//-------
			if (makingTransition) {
				//----------
				drawTransitionGame();
				//----------
			}
			//-------
		}
		void drawTransitionGame(){
			//-----------------
			DrawRectangleRec(transitionRec, colorTransition);
			if (!toMenuAfterDefeat) {
				DrawText(FormatText("SOBREVIVISTE A %i ENEMIGOS", timesPlayerWin),
					(int)(ScreenManager::actualRes.WIDTH / 4.5f), (int)(ScreenManager::actualRes.HEIGHT / 2),
					50, WHITE);
			}
			//-----------------
		}
		//-----------
		static void reinitSomeThings() {
			transitionRec = { 0.0f, 0.0f, (float)(ScreenManager::actualRes.WIDTH),(float)(ScreenManager::actualRes.HEIGHT) };
			makingTransition = false;
			toMenuAfterDefeat = false;
			fadeInDone = false;
		}
		//-----------
		void resizeThings(){
			//-------
			reinitSomeThings(),
			//-------
			Player::loadPlayer();
			//-------
			EnemyHandle::loadSprites();
			//-------
			GridHandle::resizeGrid();
			//-------
			SceneHandle::load();
			//-------
		}
		//-----------
		void restartAfetrExit(){
			//-----------
			Player::initPlayer();
			//-----------
			EnemyHandle::init();
			//-----------
			SceneHandle::init();
			//-----------
			SfxManager::stopMusic(MusicHandle::gameSongs[SfxManager::whatTrackGameplay].track);
			//-----------
		}
		//-----------
	}
}