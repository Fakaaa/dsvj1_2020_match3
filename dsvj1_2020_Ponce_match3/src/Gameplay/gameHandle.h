#ifndef GAMEHANDLE_H
#define GAMEHANDLE_H

namespace Match3 {
	namespace GameHandle {
		//-----------
		extern bool inPauseScreen;
		extern bool makingTransition;
		extern bool toMenuAfterDefeat;
		//-----------
		void makeTransitionToNext();
		//-----------
		void makeTransWhenDeadP1();
		//-----------
		void makeTransWhenDeadP2();
		//-----------
		void initGameThings();
		//-----------
		void deinitGameThings();
		//-----------
		void updateGame();
		//-----------
		void inputsGame();
		//-----------
		void drawGame();
		//-----------
		void drawTransitionGame();
		//-----------
		void resizeThings();
		//-----------
		void restartAfetrExit();
		//-----------
	}
}
#endif // !GAMEHANDLE_H