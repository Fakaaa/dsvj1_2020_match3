#include "buttons.h"

#include "ScreenHandle/ScreenHandle.h"
#include "Gameplay/Player/Player.h"
#include "Menu/menuHandle.h"
#include "Sfx/sfxHandle.h"
#include "gameLoop.h"

using namespace Match3;
using namespace ScreenManager;

namespace Match3 {
	namespace ButtonsHandle {
		//=========================
		BUTTONS START;
		//==========
		BUTTONS OPTIONS;
		//-----------------
			BUTTONS HELP;
			BUTTONS MUSIC;
			BUTTONS SCREEN;
			BUTTONS BACK;
		//==========
		BUTTONS CREDITS;
		//==========
		BUTTONS EXIT;
		//==========
		BUTTONS _800X600;
		BUTTONS _1240X720;
		BUTTONS _1440X900;
		//==========
		BALL_SCROLL scrollSounds;
		BALL_SCROLL scrollMusic;
		SCROLL_UX scrollUXSound;
		SCROLL_UX scrollUXMusic;
		//==========
		static Texture2D helpScreen;
		static Texture2D creditsScreen;
		//==========
		static float WIDTH;
		static float HEIGTH;
		//==========
		static bool wasOnOptions = false;
		static bool wasOnScreen = false;
		static bool wasOn800x600 = false;
		static bool wasOn1240x720 = false;
		static float delayFromOptions;
		static float delayFromScreen;
		static float delayFromResolution;
		static float timePassed = 0.0f;
		//==========
		static float toPlaceBttnInHalf = 2.0f;
		//==========
		static Vector2 auxPosBack;
		//==========
		float valueToScrollers;
		//==========
		Color colorButtons;
		//=========================
		void init(){
			//------------
			colorButtons = WHITE;
			//------------
			WIDTH = static_cast<float>((actualRes.WIDTH / 3) - ((actualRes.WIDTH / 4.1f) / 2));		//	262
			HEIGTH = static_cast<float>((actualRes.HEIGHT / 4) - ((actualRes.HEIGHT / 3.6f) / 2));	//  80
			valueToScrollers = static_cast<float>(HEIGTH/2);										//  40
			//====================================================
			loadTexture();
			//====================================================
			START.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)),  static_cast<float>(actualRes.HEIGHT / 5) };
			OPTIONS.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			CREDITS.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			EXIT.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.2f) };
			//------------
			START.state = false;
			START.in = false;
			//---
			OPTIONS.state = false;
			OPTIONS.in = false;
			//---
			CREDITS.state = false;
			CREDITS.in = false;
			//---
			EXIT.state = false;
			EXIT.in = false;
			//------------
			START.collider = { START.posText.x, START.posText.y , WIDTH , HEIGTH};
			OPTIONS.collider = { OPTIONS.posText.x, OPTIONS.posText.y , WIDTH , HEIGTH};
			CREDITS.collider = { CREDITS.posText.x, CREDITS.posText.y , WIDTH , HEIGTH};
			EXIT.collider = { EXIT.posText.x, EXIT.posText.y , WIDTH , HEIGTH};
			//====================================================
			HELP.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (HELP.BTN->width / 2)),   static_cast<float>(actualRes.HEIGHT / 5) };
			MUSIC.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (MUSIC.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			SCREEN.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			BACK.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (BACK.BTN->width / 2)),	static_cast<float>(actualRes.HEIGHT / 1.4f) };
			//------------
			HELP.state = false;
			HELP.in = false;
			//---
			MUSIC.state = false;
			MUSIC.in = false;
			//---
			SCREEN.state = false;
			SCREEN.in = false;
			//---
			BACK.state = false;
			BACK.in = false;
			//------------
			HELP.collider = { HELP.posText.x , HELP.posText.y , WIDTH , HEIGTH };
			MUSIC.collider = { MUSIC.posText.x , MUSIC.posText.y , WIDTH , HEIGTH };
			SCREEN.collider = { SCREEN.posText.x , SCREEN.posText.y , WIDTH , HEIGTH };
			BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
			//====================================================
			_800X600.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 5) };
			_1240X720.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			_1440X900.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			//------------
			_800X600.state = false;
			_1240X720.state = false;
			_1440X900.state = false;
			//------------
			_800X600.collider = {  _800X600.posText.x,  _800X600.posText.y ,  WIDTH, HEIGTH};
			_1240X720.collider = { _1240X720.posText.x, _1240X720.posText.y , WIDTH, HEIGTH};
			_1440X900.collider = { _1440X900.posText.x, _1440X900.posText.y , WIDTH, HEIGTH};
			//------------
			delayFromOptions = 0.5f;
			delayFromScreen = 0.5f;
			delayFromResolution = 0.5f;
			//------------
			auxPosBack = BACK.posText;
			//=====================================================
			scrollMusic.state = false;
			scrollSounds.state = false;
			scrollUXMusic.active = false;
			scrollUXSound.active = false;

			scrollUXMusic.position = { (float)((actualRes.WIDTH / 2)- (scrollUXMusic.scroll.width / 2)),
									   (float)((actualRes.HEIGHT / 4)- (scrollUXMusic.scroll.height / 2)) };

			scrollUXSound.position = { (float)((actualRes.WIDTH / 2)- (scrollUXSound.scroll.width / 2)),
									   (float)((actualRes.HEIGHT / 2)  - (scrollUXSound.scroll.height / 2)) };


			scrollMusic.posBALLTexture = { (float)(scrollUXMusic.position.x + (scrollUXMusic.scroll.width / COUNT_TRIGGERS)),
			(float)((scrollUXMusic.position.y) + (scrollUXMusic.scroll.height / 1.5f))};
			
			scrollSounds.posBALLTexture = { (float)(scrollUXSound.position.x + (scrollUXSound.scroll.width / COUNT_TRIGGERS)),
			(float)((scrollUXSound.position.y) + (scrollUXSound.scroll.height / 1.5f)) };

			scrollMusic.collider = { static_cast<float>(scrollMusic.posBALLTexture.x), static_cast<float>(scrollMusic.posBALLTexture.y) ,
									static_cast<float>(scrollMusic.texture.width),static_cast<float>(scrollMusic.texture.height) };
			scrollSounds.collider = { static_cast<float>(scrollSounds.posBALLTexture.x), static_cast<float>(scrollSounds.posBALLTexture.y) ,
									static_cast<float>(scrollSounds.texture.width),static_cast<float>(scrollSounds.texture.height) };

			scrollSounds.center = {(float)(scrollSounds.posBALLTexture.x)+(scrollSounds.texture.width / 2),
								   (float)(scrollSounds.posBALLTexture.y)+(scrollSounds.texture.height/ 2)};
			scrollMusic.center = { (float)(scrollMusic.posBALLTexture.x)+(scrollMusic.texture.width / 2),
								   (float)(scrollMusic.posBALLTexture.y)+(scrollMusic.texture.height/ 2)};

			for (int i = 0; i < COUNT_TRIGGERS; i++)
			{
				scrollUXMusic.colliders[i] = {(float)(scrollMusic.collider.x + (i*scrollMusic.texture.width)),
												(float)(scrollMusic.collider.y), (float)(scrollMusic.collider.width),
													(float)(scrollMusic.collider.height)};
				scrollUXSound.colliders[i] = { (float)(scrollSounds.collider.x + (i*scrollSounds.texture.width)),
												(float)(scrollSounds.collider.y), (float)(scrollSounds.collider.width),
													(float)(scrollSounds.collider.height) };
			}
			//=====================================================
		}
		//=========================
		void draw(){
			//--------------------
			#if DEBUG
				DrawRectangleLines((int)_800X600.collider.x, (int)_800X600.collider.y, (int)_800X600.collider.width, (int)_800X600.collider.height, GREEN);
				DrawRectangleLines((int)_1240X720.collider.x, (int)_1240X720.collider.y, (int)_1240X720.collider.width, (int)_1240X720.collider.height, RED);
				DrawRectangleLines((int)_1440X900.collider.x, (int)_1440X900.collider.y, (int)_1440X900.collider.width, (int)_1440X900.collider.height, VIOLET);
				DrawRectangleLines((int)BACK.collider.x, (int)BACK.collider.y, (int)BACK.collider.width, (int)BACK.collider.height, BLUE);
			#endif
			//--------------------
			if (!OPTIONS.state && !SCREEN.state && !CREDITS.state) {
				//====================
				if (!START.in && !START.state)
					DrawTextureEx(START.BTN[0], START.posText, 0.0f, 1.0f, colorButtons);
				else if(START.in && !START.state)
					DrawTextureEx(START.BTN[1], START.posText, 0.0f, 1.0f, colorButtons);
				if (START.state) {
					DrawTextureEx(START.BTN[2], START.posText, 0.0f, 1.0f, colorButtons);
					MenuHandle::onMenu = false;
					START.state = false;
				}
				//====================
				if (!OPTIONS.in && !OPTIONS.state)
					DrawTextureEx(OPTIONS.BTN[0], OPTIONS.posText, 0.0f, 1.0f, colorButtons);
				else if (OPTIONS.in && !OPTIONS.state)
					DrawTextureEx(OPTIONS.BTN[1], OPTIONS.posText, 0.0f, 1.0f, colorButtons);
				if (OPTIONS.state) {
					DrawTextureEx(OPTIONS.BTN[2], OPTIONS.posText, 0.0f, 1.0f, colorButtons);
				}
				//====================
				if (!CREDITS.in && !CREDITS.state)
					DrawTextureEx(CREDITS.BTN[0], CREDITS.posText, 0.0f, 1.0f, colorButtons);
				else if (CREDITS.in && !CREDITS.state)
					DrawTextureEx(CREDITS.BTN[1], CREDITS.posText, 0.0f, 1.0f, colorButtons);
				if (CREDITS.state) {
					DrawTextureEx(CREDITS.BTN[2], CREDITS.posText, 0.0f, 1.0f, colorButtons);
				}
				//====================
				if (!EXIT.in && !EXIT.state)
					DrawTextureEx(EXIT.BTN[0], EXIT.posText, 0.0f, 1.0f, colorButtons);
				else if (EXIT.in && !EXIT.state)
					DrawTextureEx(EXIT.BTN[1], EXIT.posText, 0.0f, 1.0f, colorButtons);
				if (EXIT.state) {
					DrawTextureEx(EXIT.BTN[2], EXIT.posText, 0.0f, 1.0f, colorButtons);
					GameLoop::inGame = false;
					EXIT.state = false;
				}
				//====================
			}
			else if(OPTIONS.state && !SCREEN.state && !MUSIC.state && !CREDITS.state){
				//====================
				if (!HELP.state) {
					//===================
					if (!HELP.in && !HELP.state)
						DrawTextureEx(HELP.BTN[0], HELP.posText, 0.0f, 1.0f, colorButtons);
					else if (HELP.in && !HELP.state)
						DrawTextureEx(HELP.BTN[1], HELP.posText, 0.0f, 1.0f, colorButtons);
					if (HELP.state) {
						DrawTextureEx(HELP.BTN[2], HELP.posText, 0.0f, 1.0f, colorButtons);
					}
					//====================
					if (!MUSIC.in && !MUSIC.state)
						DrawTextureEx(MUSIC.BTN[0], MUSIC.posText, 0.0f, 1.0f, colorButtons);
					else if (MUSIC.in && !MUSIC.state)
						DrawTextureEx(MUSIC.BTN[1], MUSIC.posText, 0.0f, 1.0f, colorButtons);
					if (MUSIC.state) {
						DrawTextureEx(MUSIC.BTN[2], MUSIC.posText, 0.0f, 1.0f, colorButtons);
						MUSIC.state = false;
						SfxManager::updateMasterVolume();
					}
					//====================
					if (!SCREEN.in && !SCREEN.state)
						DrawTextureEx(SCREEN.BTN[0], SCREEN.posText, 0.0f, 1.0f, colorButtons);
					else if (SCREEN.in && !SCREEN.state)
						DrawTextureEx(SCREEN.BTN[1], SCREEN.posText, 0.0f, 1.0f, colorButtons);
					if (SCREEN.state) {
						DrawTextureEx(SCREEN.BTN[2], SCREEN.posText, 0.0f, 1.0f, colorButtons);
						SCREEN.state = false;
					}
					//====================
				}
				else {
					DrawTexture(helpScreen, 0, 0, WHITE);
					BACK.posText = { (float)(actualRes.WIDTH / 8), (float)(actualRes.HEIGHT / 1.2f) };
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
				}
				if (!BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[0], BACK.posText, 0.0f, 1.0f, colorButtons);
				else if (BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[1], BACK.posText, 0.0f, 1.0f, colorButtons);
				if (BACK.state) {
					DrawTextureEx(BACK.BTN[2], BACK.posText, 0.0f, 1.0f, colorButtons);
					BACK.posText = auxPosBack;
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
					OPTIONS.state = false;
					HELP.state = false;
					BACK.state = false;
				}
				//====================
			}
			else if (OPTIONS.state && SCREEN.state && !HELP.state && !MUSIC.state && !CREDITS.state) {
				//===================
				if (!_800X600.in && !_800X600.state)
					DrawTextureEx(_800X600.BTN[0], _800X600.posText, 0.0f, 1.0f, colorButtons);
				else if (_800X600.in && !_800X600.state)
					DrawTextureEx(_800X600.BTN[1], _800X600.posText, 0.0f, 1.0f, colorButtons);
				if (_800X600.state) {
					DrawTextureEx(_800X600.BTN[2], _800X600.posText, 0.0f, 1.0f, colorButtons);
					//-------
					resizeWindow(ScreenManager::_800X600);
					//-------
					if (onResizeWindow)
						GameLoop::resizeGame();
					//-------
					_800X600.state = false;
				}
				//====================
				if (!_1240X720.in && !_1240X720.state)
					DrawTextureEx(_1240X720.BTN[0], _1240X720.posText, 0.0f, 1.0f, colorButtons);
				else if (_1240X720.in && !_1240X720.state)
					DrawTextureEx(_1240X720.BTN[1], _1240X720.posText, 0.0f, 1.0f, colorButtons);
				if (_1240X720.state) {
					DrawTextureEx(_1240X720.BTN[2], _1240X720.posText, 0.0f, 1.0f, colorButtons);
					//-------
					resizeWindow(ScreenManager::_1240X720);
					//-------
					if(onResizeWindow)
						GameLoop::resizeGame();
					//-------
					_1240X720.state = false;
				}
				//====================
				if (!_1440X900.in && !_1440X900.state)
					DrawTextureEx(_1440X900.BTN[0], _1440X900.posText, 0.0f, 1.0f, colorButtons);
				else if (_1440X900.in && !_1440X900.state)
					DrawTextureEx(_1440X900.BTN[1], _1440X900.posText, 0.0f, 1.0f, colorButtons);
				if (_1440X900.state) {
					DrawTextureEx(_1440X900.BTN[2], _1440X900.posText, 0.0f, 1.0f, colorButtons);
					//-------
					resizeWindow(ScreenManager::_1440X900);
					//-------
					if (onResizeWindow)
						GameLoop::resizeGame();
					//-------
					_1440X900.state = false;
				}
				//====================
				if (!BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[0], BACK.posText, 0.0f, 1.0f, colorButtons);
				else if (BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[1], BACK.posText, 0.0f, 1.0f, colorButtons);
				if (BACK.state) {
					DrawTextureEx(BACK.BTN[2], BACK.posText, 0.0f, 1.0f, colorButtons);
					BACK.posText = auxPosBack;
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
					SCREEN.state = false;
					BACK.state = false;
				}
				//====================
			}
			else if (OPTIONS.state && MUSIC.state && !HELP.state && !CREDITS.state) {
				drawMusicOptions();
				//====================
				if (!BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[0], BACK.posText, 0.0f, 1.0f, colorButtons);
				else if (BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[1], BACK.posText, 0.0f, 1.0f, colorButtons);
				if (BACK.state) {
					DrawTextureEx(BACK.BTN[2], BACK.posText, 0.0f, 1.0f, colorButtons);
					BACK.posText = auxPosBack;
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
					MUSIC.state = false;
					BACK.state = false;
				}
				//====================
			}
			
			if (!OPTIONS.state && CREDITS.state) {
				//====================
					BACK.posText = { (float)(actualRes.WIDTH / 1.3f), (float)(actualRes.HEIGHT / 1.2f) };
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
				//====================
					DrawTexture(creditsScreen, 0, 0, WHITE);
				//====================
				if (!BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[0], BACK.posText, 0.0f, 1.0f, colorButtons);
				else if (BACK.in && !BACK.state)
					DrawTextureEx(BACK.BTN[1], BACK.posText, 0.0f, 1.0f, colorButtons);
				if (BACK.state) {
					DrawTextureEx(BACK.BTN[2], BACK.posText, 0.0f, 1.0f, colorButtons);
					BACK.posText = auxPosBack;
					BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
					CREDITS.state = false;
					BACK.state = false;
				}
				//====================
			}
			//====================
			Player::drawCursor();
		}
		//=========================
		void drawMusicOptions(){
			//====================================================
			DrawTextureEx(scrollUXMusic.scroll, scrollUXMusic.position, 0.0f, 1.0f, WHITE);
			//====================================================
			DrawTextureEx(scrollMusic.texture, scrollMusic.posBALLTexture, 0.0f, 1.0f, GRAY);
			//====================================================
			DrawTextureEx(scrollUXSound.scroll, scrollUXSound.position, 0.0f, 1.0f, WHITE);
			//====================================================
			DrawTextureEx(scrollSounds.texture, scrollSounds.posBALLTexture, 0.0f, 1.0f, GRAY);
			//====================================================
		}
		//=========================
		void handleButtons(){
			//=====================================
			if ( wasOnOptions) {
				timePassed += GetFrameTime();
			}
			if (timePassed >= delayFromOptions) {
				timePassed = 0;
				wasOnOptions = false;
			}

			if (OPTIONS.state && wasOnScreen && SCREEN.state) {
				timePassed += GetFrameTime();
			}
			if (timePassed >= delayFromScreen) {
				timePassed = 0;
				wasOnScreen = false;
			}

			if (wasOn800x600 || wasOn1240x720) {
				timePassed += GetFrameTime();
			}
			if (timePassed >= delayFromResolution) {
				timePassed = 0;
				wasOn800x600 = false;
				wasOn1240x720 = false;
			}

			//=====================================
			Player::inputsPlayer();
			//=====================================
			if (!OPTIONS.state && !SCREEN.state && !MUSIC.state && !CREDITS.state) {
				//=====================================
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, START.collider)) {
					START.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					START.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, OPTIONS.collider)) {
					OPTIONS.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					OPTIONS.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, CREDITS.collider)) {
					CREDITS.in = true;

					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					CREDITS.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, EXIT.collider)) {
					EXIT.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					EXIT.in = false;
				}
				//=====================================
				if (START.in && Player::onTouchMenu && !wasOn800x600) {
					START.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (OPTIONS.in && Player::onTouchMenu) {
					OPTIONS.state = true;
					wasOnOptions = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (CREDITS.in && Player::onTouchMenu) {
					CREDITS.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (EXIT.in && Player::onTouchMenu && !wasOnOptions) {
					EXIT.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
			}
			else if(OPTIONS.state && !SCREEN.state && !MUSIC.state){
				//=====================================
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, HELP.collider)) {
					HELP.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					HELP.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, MUSIC.collider)) {
					MUSIC.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					MUSIC.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, SCREEN.collider)) {
					SCREEN.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					SCREEN.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, BACK.collider)) {
					BACK.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					BACK.in = false;
				}
				//=====================================
				if (HELP.in && Player::onTouchMenu) {
					HELP.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (MUSIC.in && Player::onTouchMenu && !wasOnOptions) {
					MUSIC.state = true;
					//if (SfxManager::MASTER_VOLUME > 0.0f)
					//	SfxManager::MASTER_VOLUME = 0.0;
					//else
					//	SfxManager::MASTER_VOLUME = 0.5;

					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (SCREEN.in && Player::onTouchMenu) {
					SCREEN.state = true;
					wasOnScreen = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (BACK.in && Player::onTouchMenu) {
					BACK.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
			}
			else if (OPTIONS.state && SCREEN.state && !MUSIC.state) {
				//=====================================
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, _800X600.collider)) {
					_800X600.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					_800X600.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, _1240X720.collider)) {
					_1240X720.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					_1240X720.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, _1440X900.collider)) {
					_1440X900.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					_1440X900.in = false;
				}
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, BACK.collider)) {
					BACK.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					BACK.in = false;
				}
				//=====================================
				if (_800X600.in && Player::onTouchMenu) {
					_800X600.state = true;
					wasOn800x600 = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (_1240X720.in && Player::onTouchMenu) {
					_1240X720.state = true;
					wasOn1240x720 = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (_1440X900.in && Player::onTouchMenu && !wasOnScreen) {
					_1440X900.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
				if (BACK.in && Player::onTouchMenu) {
					BACK.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
			}
			
			if (!OPTIONS.state && CREDITS.state) {
				//---
				if (CheckCollisionPointRec(Player::pj.POS_MOUSE, BACK.collider)) {
					BACK.in = true;
					SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
					SoundsHandle::playSound(SoundsHandle::passOver.sfx);
				}
				else {
					BACK.in = false;
				}
				//------
				if (BACK.in && Player::onTouchMenu) {
					BACK.state = true;
					SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
					SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
				}
				//------
			}
			//=====================================
			if (MUSIC.state) {
				handleSfxSettings();
			}
			//=====================================
		}
		//=========================
		static void updateCenterBallScroll() {
			//===================
			scrollSounds.center = { (float)(scrollSounds.posBALLTexture.x) + (scrollSounds.texture.width / 2),
								   (float)(scrollSounds.posBALLTexture.y) + (scrollSounds.texture.height / 2) };
			scrollMusic.center = { (float)(scrollMusic.posBALLTexture.x) + (scrollMusic.texture.width / 2),
								   (float)(scrollMusic.posBALLTexture.y) + (scrollMusic.texture.height / 2) };
			//===================
		}
		//=========================
		static void calcVolumeTrigger(int& intervalWherePutVolume, int WhatSetting) {
			//=========================
			switch (intervalWherePutVolume)
			{
			case 0:
				if(WhatSetting == 1)
					SfxManager::newVolumeSounds(0.0f);
				else
					SfxManager::newVolumeMusic(0.0f);
				break;
			case 1:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.1f);
				else
					SfxManager::newVolumeMusic(0.1f);
				break;
			case 2:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.2f);
				else
					SfxManager::newVolumeMusic(0.2f);
				break;
			case 3:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.3f);
				else
					SfxManager::newVolumeMusic(0.3f);
				break;
			case 4:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.4f);
				else
					SfxManager::newVolumeMusic(0.4f);
				break;
			case 5:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.5f);
				else
					SfxManager::newVolumeMusic(0.5f);
				break;
			case 6:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.6f);
				else
					SfxManager::newVolumeMusic(0.6f);
				break;
			case 7:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.7f);
				else
					SfxManager::newVolumeMusic(0.7f);
				break;
			case 8:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.8f);
				else
					SfxManager::newVolumeMusic(0.8f);
				break;
			case 9:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(0.9f);
				else
					SfxManager::newVolumeMusic(0.9f);
				break;
			case 10:
				if (WhatSetting == 1)
					SfxManager::newVolumeSounds(1.0f);
				else
					SfxManager::newVolumeMusic(1.0f);
				break;
			}
			//=========================
		}
		//=========================
		void handleSfxSettings(){
			//====================================================
			scrollMusic.collider.x = scrollMusic.posBALLTexture.x;
			scrollMusic.collider.y = scrollMusic.posBALLTexture.y;

			scrollSounds.collider.x = scrollSounds.posBALLTexture.x;
			scrollSounds.collider.y = scrollSounds.posBALLTexture.y;
			//====================================================

			if (CheckCollisionPointRec(Player::pj.POS_MOUSE, scrollMusic.collider) && Player::onTouchMenu) {
				//---------------
				if(Player::pj.POS_MOUSE.x <=((scrollUXMusic.position.x + scrollUXMusic.scroll.width) - (valueToScrollers * 1.5f)) &&
				   Player::pj.POS_MOUSE.x >= (scrollUXMusic.position.x + valueToScrollers))
					scrollMusic.posBALLTexture.x = (Player::pj.POS_MOUSE.x - 9);
				//---------------
				updateCenterBallScroll();
				//====================================================

				for (int i = 0; i < COUNT_TRIGGERS; i++)
				{
					if (CheckCollisionPointRec(scrollMusic.center, scrollUXMusic.colliders[i])) {
						scrollMusic.onWhatInterval = i;
						break;
					}
				}
				calcVolumeTrigger(scrollMusic.onWhatInterval, 2);	//2 IS MUSIC
				//====================================================	
			}

			if (CheckCollisionPointRec(Player::pj.POS_MOUSE, scrollSounds.collider) && Player::onTouchMenu) {
				//---------------
				if (Player::pj.POS_MOUSE.x <= ((scrollUXSound.position.x + scrollUXSound.scroll.width) - (valueToScrollers* 1.5f)) &&
					Player::pj.POS_MOUSE.x >= (scrollUXSound.position.x + valueToScrollers))
					scrollSounds.posBALLTexture.x = (Player::pj.POS_MOUSE.x - 9);
				//---------------
				updateCenterBallScroll();
				//====================================================
				for (int i = 0; i < COUNT_TRIGGERS; i++) {
					if (CheckCollisionPointRec(scrollSounds.center, scrollUXSound.colliders[i])) {
						scrollSounds.onWhatInterval = i;
						break;
					}
				}
				calcVolumeTrigger(scrollSounds.onWhatInterval, 1);	//1 IS SOUNDS
				//====================================================	
			}
			//====================================================		BACK TO MAIN MENU
			if (CheckCollisionPointRec(Player::pj.POS_MOUSE, BACK.collider)) {
				BACK.in = true;
				SoundsHandle::stopSound(SoundsHandle::passOver.sfx);
				SoundsHandle::playSound(SoundsHandle::passOver.sfx);
			}
			else {
				BACK.in = false;
			}
			if (BACK.in && Player::onTouchMenu) {
				BACK.state = true;
				SoundsHandle::stopSound(SoundsHandle::clickThat.sfx);
				SoundsHandle::playSound(SoundsHandle::clickThat.sfx);
			}
			//====================================================
		}
		//=========================
		static void resizeBttns() {
			//====================================================		MAIN MENU
			START.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)),  static_cast<float>(actualRes.HEIGHT / 5) };
			OPTIONS.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			CREDITS.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			EXIT.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (START.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.2f) };
			//------------
			START.state = false;
			START.in = false;
			//---
			OPTIONS.state = false;
			OPTIONS.in = false;
			//---
			CREDITS.state = false;
			CREDITS.in = false;
			//---
			EXIT.state = false;
			EXIT.in = false;
			//------------
			START.collider = { START.posText.x, START.posText.y , WIDTH , HEIGTH };
			OPTIONS.collider = { OPTIONS.posText.x, OPTIONS.posText.y , WIDTH , HEIGTH };
			CREDITS.collider = { CREDITS.posText.x, CREDITS.posText.y , WIDTH , HEIGTH };
			EXIT.collider = { EXIT.posText.x, EXIT.posText.y , WIDTH , HEIGTH };
			//====================================================		OPTIONS MENU
			HELP.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (HELP.BTN->width / 2)),   static_cast<float>(actualRes.HEIGHT / 5) };
			MUSIC.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (MUSIC.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			SCREEN.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			BACK.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (BACK.BTN->width / 2)),	static_cast<float>(actualRes.HEIGHT / 1.4f) };
			//------------
			HELP.state = false;
			HELP.in = false;
			//---
			MUSIC.state = false;
			MUSIC.in = false;
			//---
			SCREEN.state = false;
			SCREEN.in = false;
			//---
			BACK.state = false;
			BACK.in = false;
			//------------
			HELP.collider = { HELP.posText.x , HELP.posText.y , WIDTH , HEIGTH };
			MUSIC.collider = { MUSIC.posText.x , MUSIC.posText.y , WIDTH , HEIGTH };
			SCREEN.collider = { SCREEN.posText.x , SCREEN.posText.y , WIDTH , HEIGTH };
			BACK.collider = { BACK.posText.x , BACK.posText.y , WIDTH , HEIGTH };
			//====================================================		RESOLUTIONS BUTTONS
			_800X600.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 5) };
			_1240X720.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 2.7f) };
			_1440X900.posText = { static_cast<float>((actualRes.WIDTH / toPlaceBttnInHalf) - (SCREEN.BTN->width / 2)), static_cast<float>(actualRes.HEIGHT / 1.8f) };
			//------------
			_800X600.state = false;
			_1240X720.state = false;
			_1440X900.state = false;
			//------------
			_800X600.collider = { _800X600.posText.x, _800X600.posText.y , WIDTH, HEIGTH };
			_1240X720.collider = { _1240X720.posText.x, _1240X720.posText.y , WIDTH, HEIGTH };
			_1440X900.collider = { _1440X900.posText.x, _1440X900.posText.y , WIDTH, HEIGTH };
			//------------
			auxPosBack = BACK.posText;
			//====================================================		MUSCI SCREEN
			scrollMusic.state = false;
			scrollSounds.state = false;
			scrollUXMusic.active = false;
			scrollUXSound.active = false;

			scrollUXMusic.position = { (float)((actualRes.WIDTH / 2) - (scrollUXMusic.scroll.width / 2)),
									   (float)((actualRes.HEIGHT / 4) - (scrollUXMusic.scroll.height / 2)) };

			scrollUXSound.position = { (float)((actualRes.WIDTH / 2) - (scrollUXSound.scroll.width / 2)),
									   (float)((actualRes.HEIGHT / 2) - (scrollUXSound.scroll.height / 2)) };

			scrollMusic.posBALLTexture = { (float)(scrollUXMusic.position.x + (scrollUXMusic.scroll.width / COUNT_TRIGGERS)),
			(float)((scrollUXMusic.position.y) + (scrollUXMusic.scroll.height / 1.5f)) };

			scrollSounds.posBALLTexture = { (float)(scrollUXSound.position.x + (scrollUXSound.scroll.width / COUNT_TRIGGERS)),
			(float)((scrollUXSound.position.y) + (scrollUXSound.scroll.height / 1.5f)) };

			scrollMusic.collider = { static_cast<float>(scrollMusic.posBALLTexture.x), static_cast<float>(scrollMusic.posBALLTexture.y) ,
									static_cast<float>(scrollMusic.texture.width),static_cast<float>(scrollMusic.texture.height) };
			scrollSounds.collider = { static_cast<float>(scrollSounds.posBALLTexture.x), static_cast<float>(scrollSounds.posBALLTexture.y) ,
									static_cast<float>(scrollSounds.texture.width),static_cast<float>(scrollSounds.texture.height) };
			scrollSounds.center = { (float)(scrollSounds.posBALLTexture.x) + (scrollSounds.texture.width / 2),
								   (float)(scrollSounds.posBALLTexture.y) + (scrollSounds.texture.height / 2) };
			scrollMusic.center = { (float)(scrollMusic.posBALLTexture.x) + (scrollMusic.texture.width / 2),
								   (float)(scrollMusic.posBALLTexture.y) + (scrollMusic.texture.height / 2) };

			for (int i = 0; i < COUNT_TRIGGERS; i++)
			{
				scrollUXMusic.colliders[i] = { (float)(scrollMusic.collider.x + (i*scrollMusic.texture.width)),
												(float)(scrollMusic.collider.y), (float)(scrollMusic.collider.width),
													(float)(scrollMusic.collider.height) };
				scrollUXSound.colliders[i] = { (float)(scrollSounds.collider.x + (i*scrollSounds.texture.width)),
												(float)(scrollSounds.collider.y), (float)(scrollSounds.collider.width),
													(float)(scrollSounds.collider.height) };
			}
			//=====================================================
		}
		//=========================
		void loadTexture() {
			//----------
			Image resize;
			//----------
			if (!ScreenManager::onResizeWindow) {
			//----
				//=================================
				//BUTTONS MENU
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++){
					resize = LoadImage(FormatText("res/assets/buttons/start%i.png",i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					START.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/options%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					OPTIONS.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++){
					resize = LoadImage(FormatText("res/assets/buttons/credits%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					CREDITS.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/exit%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					EXIT.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/back%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					BACK.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/help%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					HELP.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/music%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					MUSIC.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/screen%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					SCREEN.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/lowRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_800X600.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/midRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_1240X720.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/higRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_1440X900.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				//HELP SCREEN
				//=================================
				resize = LoadImage("res/assets/menu/helpScreen.png");
				ImageResize(&resize, (int)actualRes.WIDTH, (int)actualRes.HEIGHT);
				helpScreen = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=================================
				//MUSIC & SOUNDS
				resize = LoadImage("res/assets/menu/circleToMove.png");
				ImageResize(&resize, (int)(WIDTH / 6.59f), (int)(HEIGTH / 2));
				scrollMusic.texture = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/circleToMove.png");
				ImageResize(&resize, (int)(WIDTH / 6.59f), (int)(HEIGTH / 2));
				scrollSounds.texture = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/soundsBar.png");
				ImageResize(&resize, (int)(WIDTH *2), (int)HEIGTH * 2);
				scrollUXSound.scroll = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/musicBar.png");
				ImageResize(&resize, (int)(WIDTH * 2), (int)HEIGTH * 2);
				scrollUXMusic.scroll = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/creditsScreen.png");
				ImageResize(&resize, (int)actualRes.WIDTH, (int)actualRes.HEIGHT);
				creditsScreen = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=================================
			//----
			}
			else{
				//----
				WIDTH = static_cast<float>((actualRes.WIDTH / 3) - ((actualRes.WIDTH/4.1f) / 2));
				HEIGTH = static_cast<float>((actualRes.HEIGHT / 4) - ((actualRes.HEIGHT / 3.6f) / 2));
				//----
				unloadTexture();
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/start%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					START.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/options%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					OPTIONS.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/credits%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					CREDITS.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/exit%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					EXIT.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/back%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					BACK.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/help%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					HELP.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/music%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					MUSIC.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/screen%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					SCREEN.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/lowRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_800X600.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/midRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_1240X720.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				for (int i = 0; i < STATE_BUTTON; i++) {
					resize = LoadImage(FormatText("res/assets/buttons/higRes%i.png", i));
					ImageResize(&resize, (int)WIDTH, (int)HEIGTH);
					_1440X900.BTN[i] = LoadTextureFromImage(resize);
					UnloadImage(resize);
				}
				//=================================
				//HELP SCREEN
				//=================================
				resize = LoadImage("res/assets/menu/helpScreen.png");
				ImageResize(&resize, (int)actualRes.WIDTH, (int)actualRes.HEIGHT);
				helpScreen = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=================================
				//MUSIC & SOUNDS
				resize = LoadImage("res/assets/menu/circleToMove.png");
				ImageResize(&resize, (int)(WIDTH / 6.59f), (int)(HEIGTH / 2));
				scrollMusic.texture = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/circleToMove.png");
				ImageResize(&resize, (int)(WIDTH / 6.59f), (int)(HEIGTH / 2));
				scrollSounds.texture = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/soundsBar.png");
				ImageResize(&resize, (int)(WIDTH * 2), (int)HEIGTH * 2);
				scrollUXSound.scroll = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/musicBar.png");
				ImageResize(&resize, (int)(WIDTH * 2), (int)HEIGTH * 2);
				scrollUXMusic.scroll = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------
				resize = LoadImage("res/assets/menu/creditsScreen.png");
				ImageResize(&resize, (int)actualRes.WIDTH, (int)actualRes.HEIGHT);
				creditsScreen = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//=================================
				resizeBttns();
				//----
			}
			//----------
		}
		//=========================
		void unloadTexture(){
			//------------
			//BUTTONS
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(START.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(OPTIONS.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(CREDITS.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(EXIT.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(BACK.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(HELP.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(MUSIC.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(SCREEN.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(_800X600.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(_1240X720.BTN[i]); }
			//------------
			for (int i = 0; i < STATE_BUTTON; i++) { UnloadTexture(_1440X900.BTN[i]); }
			//------------
			//SCREENS
			//------------
			UnloadTexture(helpScreen);
			UnloadTexture(scrollMusic.texture);
			UnloadTexture(scrollSounds.texture);
			UnloadTexture(creditsScreen);
			//------------
		}
		//=========================
		void deinit(){
			//-----
			unloadTexture();
			//-----
		}
		//=========================
	}
}