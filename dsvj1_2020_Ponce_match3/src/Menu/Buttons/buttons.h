#ifndef BUTTONS_H
#define BUTTONS_H

#include "raylib.h"

namespace Match3 {
	namespace ButtonsHandle {
		//---------
		const int STATE_BUTTON = 3;
		//---------
		//=========================
		struct BUTTONS{
			Texture2D BTN[STATE_BUTTON];
			Vector2 posText; 
			Rectangle collider;
			bool state;
			bool in;
		};
		//=========================
		const int COUNT_TRIGGERS = 11;	// FROM 0.0 TO 1.0
		//-----------------
		struct SCROLL_UX
		{
			Texture2D scroll;
			Vector2 position;
			Rectangle colliders[COUNT_TRIGGERS];
			
			bool active;
			int whatInterval;
		};
		//-----------------
		struct BALL_SCROLL
		{
			Vector2 posBALLTexture;
			Vector2 center;
			Texture2D texture;
			Rectangle collider;

			bool state;
			int onWhatInterval;
		};
		//=========================
		extern SCROLL_UX scrollUXSound;
		extern SCROLL_UX scrollUXMusic;
		//----
		extern BALL_SCROLL scrollSounds;
		extern BALL_SCROLL scrollMusic;
		//============
		extern BUTTONS START;
		//============
		extern BUTTONS OPTIONS;
		//------------//
		extern BUTTONS HELP;
		extern BUTTONS MUSIC;
		extern BUTTONS SCREEN;
		extern BUTTONS BACK;
		//------------//
		//============
		extern BUTTONS CREDITS;
		//============
		extern BUTTONS EXIT;
		//============
		extern BUTTONS _800X600;
		extern BUTTONS _1080X720;
		extern BUTTONS _1440X900;
		//=========================
		void init();
		//=========================
		void draw();
		//=========================
		void drawMusicOptions();
		//=========================
		void handleButtons();
		//=========================
		void handleSfxSettings();
		//=========================
		void loadTexture();
		//=========================
		void unloadTexture();
		//=========================
		void deinit();
		//=========================
	}
}
#endif