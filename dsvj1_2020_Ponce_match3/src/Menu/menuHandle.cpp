#include "menuHandle.h"

#include "Buttons/buttons.h"
#include "Gameplay/Player/Player.h"
#include "ScreenHandle/ScreenHandle.h"

using namespace Match3;

namespace Match3 {
	namespace MenuHandle {
		//--------------------
		bool onMenu;
		MENU_SCENE menuScene;
		//--------------------
		void initMenuThings() {
			//----
			ButtonsHandle::init();
			//----
			loadBacground();
			//----
			onMenu = true;
		}
		//--------------------
		void deinitMenuThings() {
			//----
			ButtonsHandle::deinit();
			//----
			unloadBacground();
			//----
		}
		//--------------------
		void updateButtons() {
			//-------------
			Player::updatePlayerMouse();
			//-------------
			ButtonsHandle::handleButtons();
			//-------------
		}
		//--------------------
		static void drawBackground() {
			//------------
			DrawTexture(menuScene.background, 0, 0, WHITE);
			//------------
		}
		//--------------------
		void drawMenu() {
			//------------
			drawBackground();
			//------------
			ButtonsHandle::draw();
			//------------
		}
		//--------------------
		void loadBacground() {
			//------------------
			Image resize;
			//------------------
			if (!ScreenManager::onResizeWindow) {
				//-------------
				resize = LoadImage("res/assets/menu/menuBackground.png");
				ImageResize(&resize, (int)(ScreenManager::actualRes.WIDTH), (int)(ScreenManager::actualRes.HEIGHT));
				menuScene.background = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------------
			}
			else {
				//-------------
				unloadBacground();
				//-------------
				resize = LoadImage("res/assets/menu/menuBackground.png");
				ImageResize(&resize, (int)(ScreenManager::actualRes.WIDTH), (int)(ScreenManager::actualRes.HEIGHT));
				menuScene.background = LoadTextureFromImage(resize);
				UnloadImage(resize);
				//-------------
			}
			//------------------
		}
		//--------------------
		void unloadBacground() {
			//----------------
			UnloadTexture(menuScene.background);
			//----------------
		}
		//--------------------
		void resizeThings() {
			//-----------------
			Player::loadPlayer();
			//-----------------
			ButtonsHandle::loadTexture();
			//-----------------
			loadBacground();
			//-----------------
		}
		//--------------------
	}
}