#ifndef MENUHANDLE_H
#define MENUHANDLE_H

#include "raylib.h"

namespace Match3 {
	namespace MenuHandle {
		//--------------------
		struct MENU_SCENE
		{
			Texture2D background;

			Music menuSong;
		}; 
		//--------------------
		extern MENU_SCENE menuScene;
		//--------------------
		extern bool onMenu;
		//--------------------
		void initMenuThings();
		//--------------------
		void deinitMenuThings();
		//--------------------
		void updateButtons();
		//--------------------
		void drawMenu();
		//--------------------
		void loadBacground();
		//--------------------
		void unloadBacground();
		//--------------------
		void resizeThings();
		//--------------------
	}
}
#endif // !MENUHANDLE_H