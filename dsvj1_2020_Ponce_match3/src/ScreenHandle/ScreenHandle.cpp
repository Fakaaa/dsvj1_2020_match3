#include <iostream>

#include "ScreenHandle.h"

namespace Match3 {
	namespace ScreenManager {
		//--------
		RESOLUTIONS actualRes; 
		//--------
		bool onResizeWindow;
		int resolu = 0;
		int maxFPS;
		//--------
		//----------------------------------
		void initWindow() {
			//--------
			actualRes.RES_SIZE = _1240X720;
			//--------
			switch (actualRes.RES_SIZE)
			{
			case _800X600:
				actualRes.WIDTH = 800;
				actualRes.HEIGHT = 600;
				resolu = 0;
				break;
			case _1240X720:
				actualRes.WIDTH = 1240;
				actualRes.HEIGHT = 720;
				resolu = 1;
				break;
			case _1440X900:
				actualRes.WIDTH = 1440;
				actualRes.HEIGHT = 900;
				resolu = 2;
				break;
			}
			//--------
			maxFPS = 120;
			//--------
			InitWindow(actualRes.WIDTH,actualRes.HEIGHT,"Match-Them-Up - v0.3.0");
			//--------
			SetTargetFPS(maxFPS);
			//--------
		}
		//----------------------------------
		void deinitWindow() {
			//--------
			CloseWindow();
			//--------
		}
		//----------------------------------
		void resizeWindow(TYPE_RES whatRes){
			//--------
			onResizeWindow = true;
			//--------
			switch (whatRes)
			{
			case Match3::ScreenManager::_800X600:
				actualRes.WIDTH = 800;
				actualRes.HEIGHT = 600;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			case Match3::ScreenManager::_1240X720:
				actualRes.WIDTH = 1240;
				actualRes.HEIGHT = 720;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			case Match3::ScreenManager::_1440X900:
				actualRes.WIDTH = 1440;
				actualRes.HEIGHT = 900;
				SetWindowSize(actualRes.WIDTH, actualRes.HEIGHT);
				break;
			default:
				std::cout << "ERROR01: Warning! The resize of the window fail!" << std::endl;
				break;
			}
			//--------
		}
		//----------------------------------
	}
}