#ifndef SCREENHANDLE_H
#define SCREENHANDLE_H

#include "raylib.h"

namespace Match3 {
	namespace ScreenManager {
		//--------
		enum TYPE_RES{
			_800X600,
			_1240X720,
			_1440X900
		};
		//--------
		struct RESOLUTIONS{
			int WIDTH;
			int HEIGHT;
			TYPE_RES RES_SIZE;
		};
		//--------
		extern RESOLUTIONS actualRes;
		//--------
		extern bool onResizeWindow;
		//--------
		extern int resolu;
		//--------
		extern int maxFPS;
		//--------
		void initWindow();
		//--------
		void deinitWindow();
		//--------
		void resizeWindow(TYPE_RES whatRes);
		//--------
	}
}
#endif // !SCREENHANDLE_H