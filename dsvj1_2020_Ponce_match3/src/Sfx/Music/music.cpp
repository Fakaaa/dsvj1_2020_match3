#include "music.h"

namespace Match3 {
	namespace MusicHandle {
		//-----------------
		MUSIC menuSong;
		MUSIC gameSongs[gameplayTracks];
		//-----------------
		void initMusic(){
			//-------------
			menuSong.volume = 0.5f;
			gameSongs[0].volume = 0.5f;
			gameSongs[1].volume = 0.5f;
			//-------------
			loadMusic();
			//-------------
			SetMusicVolume(menuSong.track,menuSong.volume);
			SetMusicVolume(gameSongs[0].track, gameSongs[0].volume);
			SetMusicVolume(gameSongs[1].track, gameSongs[1].volume);
			//-------------
		}
		//-----------------
		void updateMusic(Music& whatTrack){
			//-----------
			UpdateMusicStream(whatTrack);
			//-----------
		}
		//-----------------
		void loadMusic(){
			//-------------
			menuSong.track = LoadMusicStream("res/assets/sfx/music/menuMusic.mp3");
			//-------------
			for (int i = 0; i < gameplayTracks; i++){
				gameSongs[i].track = LoadMusicStream(FormatText("res/assets/sfx/music/gameplayMusic%i.mp3",i));
			}
			//-------------
		}
		//-----------------
		void unloadMusic(){
			//---------------
			UnloadMusicStream(menuSong.track);
			//---------------
			for (int i = 0; i < gameplayTracks; i++) { UnloadMusicStream(gameSongs[i].track); }
			//---------------
		}
		//-----------------
		void playMusic(Music& whatTrack){
			//-------
			if(!IsMusicPlaying(whatTrack))
				PlayMusicStream(whatTrack);
			//-------
		}
		//-----------------
		void stopMusic(Music& whatTrack){
			//-------
			StopMusicStream(whatTrack);
			//-------
		}
		//-----------------
		void pauseMusic(Music& whatTrack){
			//-------
			if (IsMusicPlaying(whatTrack))
				PauseMusicStream(whatTrack);
			//-------
		}
		//-----------------
		void resumeMusic(Music & whatTrack){
			//-------
			if (!IsMusicPlaying(whatTrack))
				ResumeMusicStream(whatTrack);
			//-------
		}
		//-----------------
		void deinitMusic(){
			//------------
			unloadMusic();
			//------------
		}
		//-----------------
	}
}