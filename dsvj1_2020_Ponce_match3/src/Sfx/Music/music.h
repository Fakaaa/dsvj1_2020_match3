#ifndef MUSIC_H
#define MUSIC_H

#include "raylib.h"

namespace Match3 {
	namespace MusicHandle {
		//---------------
		const int gameplayTracks = 2;
		//---------------
		struct MUSIC
		{
			Music track;
			float volume;
		};
		//---------------
		extern MUSIC menuSong;
		extern MUSIC gameSongs[gameplayTracks];
		//---------------
		void initMusic();
		//---------------
		void updateMusic(Music& whatTrack);
		//---------------
		void loadMusic();
		//---------------
		void unloadMusic();
		//---------------
		void playMusic(Music& whatTrack);
		//---------------
		void stopMusic(Music& whatTrack);
		//---------------
		void pauseMusic(Music& whatTrack);
		//---------------
		void resumeMusic(Music& whatTrack);
		//---------------
		void deinitMusic();
	}
}
#endif // !MUSIC_H