#include "sounds.h"

namespace Match3 {
	namespace SoundsHandle {
		//--------------------
		SOUNDS passOver;
		SOUNDS clickThat;

		SOUNDS playerDeath;
		SOUNDS playerAttack;

		SOUNDS gemSelect;

		SOUNDS damageEnemy;
		//--------------------
		void initSounds(){
			//---------------
			passOver.volume = 0.5f;
			clickThat.volume = 0.5f;
			playerAttack.volume = 0.5f;
			playerDeath.volume = 0.5f;
			gemSelect.volume = 1.0f;
			damageEnemy.volume = 0.3f;
			//---------------
			loadSounds();
			//---------------
			SetSoundVolume(passOver.sfx, passOver.volume);
			SetSoundVolume(clickThat.sfx, clickThat.volume);
			SetSoundVolume(playerAttack.sfx, playerAttack.volume);
			SetSoundVolume(playerDeath.sfx, playerDeath.volume);
			SetSoundVolume(gemSelect.sfx, gemSelect.volume);
			SetSoundVolume(damageEnemy.sfx, damageEnemy.volume);
			//---------------
		}
		//--------------------
		void deinitSounds(){
			//---------
			unloadSounds();
			//---------
		}
		//--------------------
		void loadSounds(){
			//------------
			passOver.sfx = LoadSound("res/assets/sfx/sounds/onHim.ogg");
			//------------
			damageEnemy.sfx = LoadSound("res/assets/sfx/sounds/enemyAtack.ogg");
			//------------
			clickThat.sfx = LoadSound("res/assets/sfx/sounds/clickThat.ogg");
			//------------
			playerDeath.sfx = LoadSound("res/assets/sfx/sounds/playerDeath.ogg");
			//------------
			playerAttack.sfx = LoadSound("res/assets/sfx/sounds/playerAttack.ogg");
			//------------
			gemSelect.sfx = LoadSound("res/assets/sfx/sounds/selectGem.ogg");
			//------------
		}
		//--------------------
		void unloadSounds(){
			//------------
			UnloadSound(passOver.sfx);
			//------------
			UnloadSound(damageEnemy.sfx);
			//------------
			UnloadSound(clickThat.sfx);
			//------------
			UnloadSound(playerDeath.sfx);
			//------------
			UnloadSound(playerAttack.sfx);
			//------------
			UnloadSound(gemSelect.sfx);
			//------------
		}
		//--------------------
		void playSound(Sound & whatSound){
			if (!IsSoundPlaying(whatSound))
				PlaySound(whatSound);
		}
		//--------------------
		void stopSound(Sound & whatSound){
			if (IsSoundPlaying(whatSound))
				StopSound(whatSound);
		}
		//--------------------
	}
}