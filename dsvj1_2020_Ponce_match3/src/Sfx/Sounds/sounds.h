#ifndef SOUNDS_H
#define SOUNDS_H

#include "raylib.h"

namespace Match3 {
	namespace SoundsHandle {
		//------------
		struct SOUNDS
		{
			Sound sfx;
			float volume;
		};
		//------------
		extern SOUNDS passOver;
		extern SOUNDS damageEnemy;
		extern SOUNDS clickThat;

		extern SOUNDS playerDeath;
		extern SOUNDS playerAttack;

		extern SOUNDS gemSelect;
		//------------
		void initSounds();
		//------------
		void deinitSounds();
		//------------
		void loadSounds();
		//------------
		void unloadSounds();
		//------------
		void playSound(Sound& whatSound);
		//------------
		void stopSound(Sound& whatSound);
		//------------
	}
}
#endif // !SOUNDS_H