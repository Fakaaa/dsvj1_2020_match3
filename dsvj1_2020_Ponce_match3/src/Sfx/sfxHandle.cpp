#include "sfxHandle.h"

#include "Menu/menuHandle.h"
#include "Gameplay/Scene/GameScene.h"

using namespace Match3;

namespace Match3 {
	namespace SfxManager {
		//-----------------
		int whatTrackGameplay;
		bool MUTE_ALL = false;
		bool exitUpdate = false;
		//-----------------
		float MASTER_VOLUME;
		//-----------------
		void initSfxs(){
			//-------------
			InitAudioDevice();
			//-------------
			MusicHandle::initMusic();
			//-------------
			SoundsHandle::initSounds();
			//-------------
			whatTrackGameplay = GetRandomValue(0, 1);
			//-------------
		}
		//-----------------
		void deinitSfxs(){
			//--------------
			MusicHandle::deinitMusic();
			//--------------
			SoundsHandle::deinitSounds();
			//--------------
			CloseAudioDevice();
			//--------------
		}
		//-----------------
		void updateSfx(){
			//--------------
			if(MenuHandle::onMenu && IsMusicPlaying(MusicHandle::menuSong.track))
				MusicHandle::updateMusic(MusicHandle::menuSong.track);
			else if(!MenuHandle::onMenu && IsMusicPlaying(MusicHandle::gameSongs[whatTrackGameplay].track))
				MusicHandle::updateMusic(MusicHandle::gameSongs[whatTrackGameplay].track);
			//--------------
		}
		//-----------------
		void updateMasterVolume(){
			//-----------------
			SetMasterVolume(MASTER_VOLUME);
			//-----------------
		}
		//-----------------
		void newVolumeSounds(float  newSoundVolume){
			//-----------------------
			//SOUNDS
			SetSoundVolume(SoundsHandle::clickThat.sfx, newSoundVolume);
			SetSoundVolume(SoundsHandle::passOver.sfx, newSoundVolume);
			SetSoundVolume(SoundsHandle::damageEnemy.sfx, newSoundVolume);
			SetSoundVolume(SoundsHandle::gemSelect.sfx, newSoundVolume);
			SetSoundVolume(SoundsHandle::playerAttack.sfx, newSoundVolume);
			SetSoundVolume(SoundsHandle::playerDeath.sfx, newSoundVolume);
			//------		
		}
		void newVolumeMusic(float newMusicVolume)
		{
			//-----------------------
			//MUSIC
			SetMusicVolume(MusicHandle::menuSong.track, newMusicVolume);
			SetMusicVolume(MusicHandle::gameSongs[0].track, newMusicVolume);
			SetMusicVolume(MusicHandle::gameSongs[1].track, newMusicVolume);
			//------
			//-----------------------
		}
		//-----------------
		void playMusic(Music & whatTrack){
			//----
			MusicHandle::playMusic(whatTrack);
			//----
		}
		//-----------------
		void pauseMusic(Music & whatTrack){
			//----
			MusicHandle::pauseMusic(whatTrack);
			//----
		}
		//-----------------
		void stopMusic(Music & whatTrack){
			//----
			MusicHandle::stopMusic(whatTrack);
			//----
		}
		//-----------------
		void playSound(Sound & whatSound){

		}
		//-----------------
		void stopSound(Sound & whatSound){

		}
		//-----------------
	}
}