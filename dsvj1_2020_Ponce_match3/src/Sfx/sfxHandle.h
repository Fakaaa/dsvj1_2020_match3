#ifndef SFXHANDLE_H
#define SFXHANDLE_H

#include "Music/music.h"
#include "Sounds/sounds.h"

namespace Match3 {
	namespace SfxManager {
		//-----------------
		extern int whatTrackGameplay;
		extern float MASTER_VOLUME;
		//-----------------
		void initSfxs();
		//-----------------
		void deinitSfxs();
		//-----------------
		void updateSfx();
		//-----------------
		void updateMasterVolume();
		//-----------------
		void newVolumeSounds(float newSoundVolume);
		void newVolumeMusic(float newMusicVolume);
		//-----------------
		void playMusic(Music& whatTrack);
		//-----------------
		void pauseMusic(Music& whatTrack);
		//-----------------
		void stopMusic(Music& whatTrack);
		//======================
		void playSound(Sound& whatSound);
		//------------
		void stopSound(Sound& whatSound);
		//------------
	}
}
#endif // SFXHANDLE_H