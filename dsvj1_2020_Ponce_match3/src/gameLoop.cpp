#include "gameLoop.h"

#include "raylib.h"

#include "ScreenHandle/ScreenHandle.h"
#include "Gameplay/gameHandle.h"
#include "Menu/menuHandle.h"
#include "Sfx/sfxHandle.h"

using namespace Match3;

namespace Match3 {
	namespace GameLoop {
		//--------------
		bool inGame = true;
		bool firstInit;
		//--------------
		void init(){
			//-----------
			firstInit = true;
			//-----------
			ScreenManager::initWindow();
			//-----------
			GameHandle::initGameThings();
			//-----------
			MenuHandle::initMenuThings();
			//-----------
			SfxManager::initSfxs();
			//-----------
		}
		//--------------
		void deinit() {
			//-----------
			ScreenManager::deinitWindow();
			//-----------
			MenuHandle::deinitMenuThings();
			//-----------
			GameHandle::deinitGameThings();
			//-----------
			SfxManager::deinitSfxs();
			//-----------
		}
		//--------------
		void update() {
			//-----------
			//if (ScreenManager::onResizeWindow) { resizeGame(); }
			//-----------
			SfxManager::updateSfx();
			//-----------
			if (MenuHandle::onMenu) {
				//----------------
				GameHandle::makeTransWhenDeadP2();
				//----------------
				if (!GameHandle::makingTransition) {
					if (!firstInit) GameHandle::restartAfetrExit();
					//----------------
					SfxManager::stopMusic(MusicHandle::gameSongs[SfxManager::whatTrackGameplay].track);
					//----------------
					SfxManager::playMusic(MusicHandle::menuSong.track);
					//----------------
					MenuHandle::updateButtons();
					//----------------
				}
			}
			else {
				firstInit = false;
				//----------------
				SfxManager::stopMusic(MusicHandle::menuSong.track);
				//----------------
				if(!GameHandle::inPauseScreen)
					SfxManager::playMusic(MusicHandle::gameSongs[SfxManager::whatTrackGameplay].track);
				else
					SfxManager::pauseMusic(MusicHandle::gameSongs[SfxManager::whatTrackGameplay].track);
				//----------------
				GameHandle::updateGame();
				//----------------
			}
			//-----------
			if (GameHandle::toMenuAfterDefeat) {
				MenuHandle::onMenu = true;
			}
			//-----------
		}
		//--------------
		void draw() {
			//-----------
			ClearBackground(WHITE);
			//-----------
			BeginDrawing();
			//-----------
			if (MenuHandle::onMenu) {
				//---------------
				MenuHandle::drawMenu();
				//---------------
				if (GameHandle::makingTransition)
					GameHandle::drawTransitionGame();
				//---------------
			}
			else {
				//---------------
				GameHandle::drawGame();
				//---------------
			}
			//-----------
			EndDrawing();
			//-----------
		}
		//--------------
		void inputs() {
			//-----------
			GameHandle::inputsGame();
			//-----------
		}
		//--------------
		void loop(){
			//-----------
			init();
			//-----------
			while (!WindowShouldClose() && inGame) {
				//------
				inputs();
				//------
				update();
				//------
				draw();
				//------
			}
			//-----------
			deinit();
			//-----------
		}
		//--------------
		void resizeGame(){
			//-----------
			GameHandle::resizeThings();
			//-----------
			MenuHandle::resizeThings();
			//-----------
			ScreenManager::onResizeWindow = false;
		}
		//--------------
	}
}