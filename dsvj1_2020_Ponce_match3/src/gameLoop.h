#ifndef GAMELOOP_H
#define GAMELOOP_H

namespace Match3 {
	namespace GameLoop {
		//--------------
		extern bool inGame;
		//--------------
		void init();
		//--------------
		void deinit();
		//--------------
		void update();
		//--------------
		void draw();
		//--------------
		void inputs();
		//--------------
		void loop();
		//--------------
		void resizeGame();
		//--------------
	}
}
#endif // !GAMELOOP_H